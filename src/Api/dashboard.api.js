import { apiExecute } from ".";

export const dashboard = async(data) => {
    // console.log('res',data);
    let res = await apiExecute("dashboard", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}