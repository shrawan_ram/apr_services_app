import { apiExecute } from "."

export const slider_list = async(data) => {
    let res = await apiExecute("slider_list", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}