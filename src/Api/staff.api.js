import { apiExecute } from "."

export const Staff_list = async(Role) => {

    let res = await apiExecute("staff_list?role="+Role, "GET",Role, {auth: true});
    // console.log('res',res.data);
    return res;
}
export const staff_Detail = async(id) => {

    let res = await apiExecute("staff_Detail?id="+id, "GET",id, {auth: true});
    // console.log('res',res.data);
    return res;
}
export const add_staff = async(data) => {

    let res = await apiExecute("add_staff", "POST",data, {auth: true});
    // console.log('res',res.data);
    return res;
}
export const edit_staff = async(data) => {

    let res = await apiExecute("edit_staff", "POST",data, {auth: true});
    // console.log('res',res.data);
    return res;
}