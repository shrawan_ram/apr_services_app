import { apiExecute } from ".";

export const get_setting = async(data) => {
    // console.log('res',data);
    let res = await apiExecute("get_setting", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const edit_setting = async(data) => {
    // console.log('res',data);
    let res = await apiExecute("edit_setting", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}