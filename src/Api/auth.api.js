import { apiExecute } from "."

export const register = async(data) => {
    let res = await apiExecute("new_register", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const sendOtp = async(data) => {
    let res = await apiExecute("sendOtp", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const verifyOtp = async(data) => {
    // console.log('data',data);
    let res = await apiExecute("verifyOtp", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}
export const forgot_password = async(data) => {
    // console.log('data',data);
    let res = await apiExecute("forgot_password", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const login = async(data) => {
    // console.log('res',data);
    let res = await apiExecute("login", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const admin_change_password = async(data) => {
    // console.log('res',data);
    let res = await apiExecute("admin_change_password", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}
export const user_change_password = async(data) => {
    // console.log('res',data);
    let res = await apiExecute("user_change_password", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const admin_Profile = async(data) => {
    // console.log('res',data);
    let res = await apiExecute("admin_profile", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const user_Profile = async(data) => {
    // console.log('res',data);
    let res = await apiExecute("user_profile", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const admin_editProfile = async(data) => {
    // console.log('res',data);
    let res = await apiExecute("admin_edit_profile", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const user_editProfile = async(data) => {
    // console.log('res',data);
    let res = await apiExecute("user_edit_profile", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const logout = async(data) => {
    let res = await apiExecute("logout", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}