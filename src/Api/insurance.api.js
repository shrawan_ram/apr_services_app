import { apiExecute } from "."

export const admin_insurance = async(data) => {
    let res = await apiExecute("admin_insurance_list", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const user_insurance = async(data) => {
    let res = await apiExecute("user_insurance_list", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const brand_list = async(data) => {
    let res = await apiExecute("brand_list", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const plan_list = async(data) => {
    let res = await apiExecute("plan_list", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const admin_add_insurance = async(data) => {
    console.log('data',data);
    let res = await apiExecute("admin_add_insurance", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}
export const user_add_insurance = async(data) => {
    console.log('data',data);
    let res = await apiExecute("user_add_insurance", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const admin_update_insurance = async(u_data) => {
    console.log('u_data',u_data);
    let res = await apiExecute("admin_update_insurance", "POST", u_data, {auth: true});
    // console.log('res',res.u_data);
    return res;
}
export const user_update_insurance = async(u_data) => {
    console.log('u_data',u_data);
    let res = await apiExecute("user_update_insurance", "POST", u_data, {auth: true});
    // console.log('res',res.u_data);
    return res;
}