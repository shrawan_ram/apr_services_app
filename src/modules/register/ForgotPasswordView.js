import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  Alert,
  SafeAreaView,
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import { Button, RadioGroup, Dropdown } from '../../components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import SelectDropdown from 'react-native-select-dropdown';
import { brand_list, plan_list } from '../../Api/insurance.api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Icon } from "react-native-elements";
import { forgot_password } from '../../Api/auth.api';

export default class ForgotPasswordScreen extends Component {
  
    state = {
        new_pw              : '',
        confirm_pw          : '',
        new_pw_secure       : true, 
        confirm_pw_secure   : true,
        errorNewpassword    : true, 
        errorConfirmpassword    : true, 
    }
  
    ForgotPassword = async () => {
        let {route} = this.props;
        let data = {
            mobile              : route.params.mobile,
            otp_code            : route.params.otp_code,
            new_password        : this.state.new_pw,
            confirm_password    : this.state.confirm_pw,
            type                : route.params.type,
        }
        let response = '';
        if(this.state.new_pw != ''){
            this.setState({errorNewpassword: true});
          } else {
            this.setState({errorNewpassword: false});
          }
          if(this.state.confirm_pw != ''){
            this.setState({errorConfirmpassword: true});
          } else {
            this.setState({errorConfirmpassword: false});
          }
          if(this.state.new_pw == '' || this.state.confirm_pw == ''){
            
            
      
          } else {
            if(this.state.new_pw != this.state.confirm_pw){         
            
                alert('Please enter the same password in both password fields');
            } else{

                response = await forgot_password(data);
                if (response.data.status == false) {
                    alert(response.data.message);
                  }
            }
          }
    //   console.log('res',response);
    
      if(response.data.status){
        this.props.navigation.push('Login'); 
              }      
    }
    newEyePress () {
        if(this.state.new_pw_secure == true){
            this.setState({new_pw_secure : false});
        } else {
            this.setState({new_pw_secure : true});
        }
    }
    confirmEyePress () {
        if(this.state.confirm_pw_secure == true){
            this.setState({confirm_pw_secure : false});
        } else {
            this.setState({confirm_pw_secure : true});
        }
    }  
     

      async componentDidMount () {
        
      }

  render(){
    let {route} = this.props;

    const { width } = Dimensions.get('window');
    let {navigation} = this.props;
    

    return (
      <SafeAreaView >
        <StatusBar backgroundColor="#00afef" barStyle="light-content" />
        
        <View style={styles.container}>
          <View style={{marginVertical:20,justifyContent:'center'}}>
                
                <Text style={styles.label}>New Password</Text>
                <View >
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "New Password"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            secureTextEntry={this.state.new_pw_secure}
                            value={this.state.new_pw}  
                            onChangeText={new_pw => this.setState({ new_pw })}
                        
                    />
                     
                    <View  style={{position:'absolute',right:20,bottom:20}}>
                        <Icon onPress={() => this.newEyePress()} type="octicon" color='#a1a1a1' size={20}  name={this.state.new_pw_secure == true ? 'eye-closed' : 'eye'} />
                    </View>
                </View>
                {
                  !this.state.errorNewpassword ? <><Text  style={styles.errorMessage}>Please Enter a New Password</Text></> : <></>
                }
                <Text style={styles.label}>Confirm Password</Text>
                <View >
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Confirm Password"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            secureTextEntry={this.state.confirm_pw_secure}
                            value={this.state.confirm_pw}  
                            onChangeText={confirm_pw => this.setState({ confirm_pw })}
                        
                    />
                   
                    <View  style={{position:'absolute',right:20,bottom:20}}>
                        <Icon onPress={() => this.confirmEyePress()} type="octicon" color='#a1a1a1' size={20}  name={this.state.confirm_pw_secure == true ? 'eye-closed' : 'eye'} />
                    </View>
                </View>
                {
                  !this.state.errorConfirmpassword ? <><Text  style={styles.errorMessage}>Please Enter a Confirm Password</Text></> : <></>
                }
                <Button
                    style={[styles.demoButton]}
                    
            
                    caption="Change Password"
                    onPress={() => this.ForgotPassword()}
                    />
                     
                        
                </View>
        </View>
        
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // alignItems: 'center',
        // justifyContent: 'space-around',
        paddingHorizontal:15
    },
    input: {
        // margin: 15,
        marginVertical:10,
        height: 40,
        color:'#000',
        borderColor: '#00afef', 
        borderWidth: 1,
        width:'100%',
        backgroundColor:'#fff',
        borderRadius:5
    },
    passwordContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#000',
        paddingBottom: 0,
      },
      inputStyle: {
        flex: 1,
      },
      errorMessage:{
          alignSelf:'flex-start',
          color:'red',
          paddingBottom:5
      }
 });


