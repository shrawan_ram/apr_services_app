import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ImageBackground,
    StatusBar,
    ActivityIndicator,
  } from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { Button, RadioGroup, Dropdown } from '../../components';
import { sendOtp, verifyOtp } from '../../Api/auth.api';
import CodeInput from 'react-native-confirmation-code-input';
export default class OtpVerifyScreen extends Component {
  state = {
    otp_code : '',
    loading: false
  }
  _onFulfill(otp_code) {
    this.setState({otp_code:otp_code});
    
  }

  verifyOtp = async () => {
    let {route} = this.props;
    let data = {
      mobile : route.params.mobile,
      otp_code : this.state.otp_code,
      type    : route.params.type,
  }
  let response = '';
  
  if(this.state.otp_code == ''){
    
    
      alert('Please Enter OTP');
      
  }else{

  
     response = await verifyOtp(data);
     if (response.data.status == false) {
       alert(response.data.message);
     }
  }
    // console.log('response',response);
    // console.log('res',response.data.status);
    if(response.data.status){
      if(route.params.target) {
        this.props.navigation.push('ForgotPassword',data); 
      } else{
        this.props.navigation.push('Login'); 
    }

  }
}

ResendOtp = async () =>{  
  let {route} = this.props;
  
    let data = {
      mobile : route.params.mobile,
      target : 'forgot_password',
      type    : route.params.type,
  }
  console.log('data',data);
  let response = await sendOtp(data);
  if(response.data.status){
    
    this.props.navigation.push('OtpVerify',route.params)
  }
  console.log('res',response);
  
}



  async componentDidMount () {
      // this.login();
  }
  render(){
    let {route,navigation} = this.props;
    // console.log('mobile',route.params);
    // console.log('otp_code',this.state.otp_code);
    setTimeout(() => {
      this.setState({ loading: true })
    }, 1000)
    
     if (this.state.loading) {
    return(
        <View style={styles.container}>
            
      <ImageBackground
       
        style={styles.bgImage}
      >
          <StatusBar barStyle="light-content" backgroundColor="#00afef" translucent={true}/>
        <View style={styles.section}>
            <Image style={styles.image} source={require('../../../assets/images/apr_logo.png')} />
        </View>
        <View style={styles.section}>
          <Text style={{fontSize:25,fontWeight:'bold'}}>OTP Verification</Text>
          <Text style={{color:'#444b55'}}>Enter the OTP sent to</Text>
          <Text style={{fontSize:17,fontWeight:'bold'}}>{route.params.mobile}</Text>
          
        <CodeInput
              ref="codeInputRef1"
              codeLength={6}
              // secureTextEntry
              activeColor='#000'
              inactiveColor='#000'
              className={'border-b'}
              space={6}
              size={40}
              keyboardType='numeric'
              inputPosition='left'
              onFulfill={(otp_code) => this.setState({otp_code})}
              // onCodeChange={(otp_code) => this.setState({otp_code})}
            /> 
            
        <View >              
          <Text style={{color:'#444b55',marginBottom:50}}>Didn't not receive the OTP?<Text onPress={() => this.ResendOtp()} style={{fontSize:17,color:colors.primary}}> Resend OTP</Text></Text>
        </View> 
            
        </View>
        <View style={styles.section_a}>
            <Button
                style={[styles.demoButton,]}
                color="#f58634"
                rounded
                caption="OTP VERIFY"
                onPress={() => this.verifyOtp()}
            />       
        </View>
        
      </ImageBackground>
    </View>
    );
  } else {
    return (
      <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
        <ActivityIndicator  color={colors.primary} />
      </View>
    )
  }
  }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
      },
      bgImage: {
        flex: 1,
        marginHorizontal: -20,
        position:'absolute',
        left:0,
        top:0,
        right:0,
        bottom:0
      },
      section: {
        flex: 1,
        paddingHorizontal: 40,
        justifyContent: 'center',
        alignItems: 'center',
      },
      section_a: {
        flex: .4,
        paddingHorizontal: 40,
        alignItems: 'center',
      },
      sectionLarge: {
        flex: 2,
        justifyContent: 'space-around',
      },
      sectionHeader: {
        marginBottom: 8,
      },
      priceContainer: {
        alignItems: 'center',
      },
      description: {
        padding: 15,
        lineHeight: 25,
      },
      titleDescription: {
        color: '#19e7f7',
        textAlign: 'center',
        fontFamily: fonts.primaryRegular,
        fontSize: 15,
      },
      title: {
        marginTop: 30,
      },
      price: {
        marginBottom: 5,
      },
      priceLink: {
        borderBottomWidth: 1,
        borderBottomColor: colors.primary,
      },
      input: {
        margin: 15,
        height: 40,
        color:'#fff',
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
        width:'100%'
     },
     demoButton: {
        width:'100%',
        marginTop: 8,
        marginBottom: 8,
      },
      root: {flex: 1, padding: 20},
  title: {textAlign: 'center', fontSize: 30},
  codeFieldRoot: {marginTop: 20},
  cell: {
    width: 40,
    height: 40,
    lineHeight: 38,
    fontSize: 24,
    borderWidth: 2,
    borderColor: '#00000030',
    textAlign: 'center',
  },
  focusCell: {
    borderColor: '#000',
  },
  errorMessage:{
    color:'red',
    textAlign:'left',
    // alignSelf:'flex-start'
  }
})