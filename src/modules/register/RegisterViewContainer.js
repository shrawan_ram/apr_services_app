import { compose, withState } from 'recompose';
import RegisterScreen from './RegisterView';


export default compose(withState('isExtended', 'setIsExtended', false))(
  RegisterScreen,
);
