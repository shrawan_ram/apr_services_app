import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ImageBackground,
    StatusBar,
    ActivityIndicator,
  } from 'react-native';
  
import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { Button, RadioGroup, Dropdown } from '../../components';
import { register } from '../../Api/auth.api';
export default class RegisterScreen extends Component {
  state = {
    name    : '',
    email   : '',
    mobile  : '',
    password: '',
    errorMobile   : true,
    errorFname    : true,
    errorPassword : true,
    loading       : false
  }

  register = async () => {
    let data = {
      mobile        : this.state.mobile,
      
      fname         : this.state.name,
      
      password      : this.state.password,
      
  }
  // console.log('data', data);
  let response = '';
  if(this.state.mobile != ''){
    this.setState({errorMobile: true});
  } else {
    this.setState({errorMobile: false});
  }
  if(this.state.password != ''){
    this.setState({errorPassword: true});
  } else {
    this.setState({errorPassword: false});
  }
  if(this.state.name != ''){
    this.setState({errorFname: true});
  } else {
    this.setState({errorFname: false});
  }
  if(this.state.mobile == '' || this.state.password == '' || this.state.fname){
    
    
      
  }else{

    response = await register(data);
  }
    console.log('response',response);
    // console.log('res',response.data.status);
    // if (response.data.status == false) {
    //   alert(response.data.message);
    // }
    if(response.data.status){
        this.props.navigation.push('OtpVerify',data); 
    }

  }



  async componentDidMount () {
      // this.login();
  }
  render(){
    let {navigation} = this.props;
    console.log('mobile',this.state.mobile);
    console.log('name',this.state.name);
    console.log('password',this.state.errorPassword);
    setTimeout(() => {
      this.setState({ loading: true })
    }, 1000)
    
     if (this.state.loading) {
    return(
        <View style={styles.container}>
            
      <ImageBackground
        source={require('../../../assets/images/background.png')}
        style={styles.bgImage}
        resizeMode="cover"
      >
          <StatusBar barStyle="light-content" backgroundColor="transparent" translucent={true}/>
        <View style={styles.section}>
            <Image style={styles.image} source={require('../../../assets/images/apr_logo.png')} />
        </View>
        <View style={styles.section}>
            <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                placeholder = "Username"
                placeholderTextColor = "#fff"
                selectionColor={'#fff'}
                autoCapitalize = "none"
                value={this.state.name}  
                onChangeText={name => this.setState({ name })}
            />
            {
                  !this.state.errorFname ? <><Text  style={styles.errorMessage}>Please Enter a Name</Text></> : <></>
                }
            {/* <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                placeholder = "Email"
                placeholderTextColor = "#fff"
                autoCapitalize = "none"
                onChangeText = {this.handleEmail}/> */}
                
            <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                placeholder = "Mobile No."
                placeholderTextColor = "#fff"
                autoCapitalize = "none"
                selectionColor={'#fff'}
                keyboardType='numeric'
                value={this.state.mobile}  
                onChangeText={mobile => this.setState({ mobile })}
                />
              {
                  !this.state.errorMobile ? <><Text  style={styles.errorMessage}>Please Enter a Mobile No.!</Text></> : <></>
                }
            <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                placeholder = "Password"
                secureTextEntry={true}
                placeholderTextColor = "#fff"
                selectionColor={'#fff'}
                autoCapitalize = "none"
                value={this.state.password}  
                onChangeText={password => this.setState({ password })}
                />
                {
                  !this.state.errorPassword ? <><Text  style={styles.errorMessage}>Please Enter a Password</Text></> : <></>
                }
        </View>
        <View style={styles.section_a}>
            <Button
                style={[styles.demoButton,]}
                color="#f58634"
                rounded
                caption="User Register"
                onPress={() => this.register()}
            />       
        </View>
        <View style={[styles.section_a]}>
            <Text style={{color:'#fff'}}>Already have an account?
                {/* <TouchableOpacity > */}
                    <Text onPress={() => navigation.push('Login')} style={{color:'#fff',fontSize:17,paddingBottom:0}}> Login</Text>                
                {/* </TouchableOpacity> */}
            </Text>  
        </View>
      </ImageBackground>
    </View>
    );
  } else {
    return (
      <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
        <ActivityIndicator  color={colors.primary} />
      </View>
    )
  }
  }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
      },
      bgImage: {
        flex: 1,
        marginHorizontal: -20,
        position:'absolute',
        left:0,
        top:0,
        right:0,
        bottom:0
      },
      section: {
        flex: 1,
        paddingHorizontal: 40,
        justifyContent: 'center',
        alignItems: 'center',
      },
      section_a: {
        flex: .4,
        paddingHorizontal: 40,
        alignItems: 'center',
      },
      sectionLarge: {
        flex: 2,
        justifyContent: 'space-around',
      },
      sectionHeader: {
        marginBottom: 8,
      },
      priceContainer: {
        alignItems: 'center',
      },
      description: {
        padding: 15,
        lineHeight: 25,
      },
      titleDescription: {
        color: '#19e7f7',
        textAlign: 'center',
        fontFamily: fonts.primaryRegular,
        fontSize: 15,
      },
      title: {
        marginTop: 30,
      },
      price: {
        marginBottom: 5,
      },
      priceLink: {
        borderBottomWidth: 1,
        borderBottomColor: colors.primary,
      },
      input: {
        margin: 15,
        height: 40,
        color:'#fff',
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
        width:'100%'
     },
     demoButton: {
        width:'100%',
        marginTop: 8,
        marginBottom: 8,
      },
      errorMessage:{
        color:'red',
        textAlign:'left',
        alignSelf:'flex-start'
      }
})