import AsyncStorage from "@react-native-async-storage/async-storage";
import { createDrawerNavigator } from "@react-navigation/drawer";
import React, { Component } from "react";
import DashboardScreen from "../admin/DashboardScreen";
import HomeScreen from "../home/HomeView";
import CustomDrawerContent from "./CustomeDrawer";

const Drawer = createDrawerNavigator();

export default class DrawerNavigation extends Component {

    state = {
        user_detail : '',
        token:''
    }

    async componentDidMount () {
        let token = await AsyncStorage.getItem('@token');
        
        token = token !== null ? token : '';
        this.setState({token: token});
    
        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
    
        
    }

    render() {
        let token = this.state.token;
        console.log('user',this.state.user_detail);
        return (
            <Drawer.Navigator
                drawerContent={props => <CustomDrawerContent {...props} />}
                initialRouteName={ token == '' ? "Login" : "Homes"}
            >
               

{
          this.state.user_detail.role_id == '4'
          ?
          <Drawer.Screen 
            name="Homes" 
            component={HomeScreen} 
            options={{ 
            //   headerShown:false,
              title:'APR Services',  
              headerTintColor: '#fff', 
              headerStyle:{  backgroundColor:'#00afef' } 
            }}
          />          
          :
          <Drawer.Screen 
          name="Homes"
          component={DashboardScreen} 
          options={{             
            title:'Dashboard',  
            headerTintColor: '#fff', 
            headerStyle:{  backgroundColor:'#00afef' } 
          }}
        />

        }
            </Drawer.Navigator>
        );
    }
}