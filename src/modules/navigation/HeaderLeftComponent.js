import React,{ Component } from "react";
import { Image, TouchableOpacity } from "react-native";

export default class HeaderLeftComponent extends Component{
    render(){
        let onPress = this.props.onPress;
        // console.log('prop',onPress);
        return(
            <TouchableOpacity onPress={() => onPress()} style={{
                paddingHorizontal: 16,
                paddingVertical: 12,
              }}>
                <Image
                source={require('../../../assets/images/icons/arrow-back.png')}
                resizeMode="contain"
                style={{
                    height: 20,
                }}
                />
            </TouchableOpacity>
        );
    }
}