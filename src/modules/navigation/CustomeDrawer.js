import React, { Component } from "react";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import { Image, StyleSheet, Text, View } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { admin_Profile, logout, user_Profile } from "../../Api/auth.api";
import { url } from "../../configs/constants.config";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class CustomDrawerContent extends Component{
    
    state = {
        drawerData : [
            {
                name: 'Homes',
                title: 'Home',
                icon: require('../../../assets/images/drawer/home.png'),
              },
              {
                name: 'InsurancesScreen',
                title: 'Mobile Repair Services',
                icon: require('../../../assets/images/drawer/mobile-insurance.png'),
              }
              // ,
              // {
              //   name: 'Calendar',
              //   icon: require('../../../assets/images/drawer/calendar.png'),
              // },
              // {
              //   name: 'Grids',
              //   icon: require('../../../assets/images/drawer/grids.png'),
              // },
              // {
              //   name: 'Pages',
              //   icon: require('../../../assets/images/drawer/pages.png'),
              // },
              // {
              //   name: 'Components',
              //   icon: require('../../../assets/images/drawer/components.png'),
              // },
        ],
        user_detail: '',

    }

    Logout = async () => {

        let response = await logout();
        if(response.status){
            await AsyncStorage.removeItem('@user');
            await AsyncStorage.removeItem('@token');
            this.props.navigation.push('Login');
        }  
        
    }

    Profile = async () => {

        let role_id = this.state.user_detail.role_id;
        console.log('role',role_id);
        let response = '';
        if(role_id == '4'){
            response = await user_Profile();
        } else {
            response = await admin_Profile();
        }
      if(response.status){
          this.setState({user_detail: response.data.data});
              }  
      
      }

    async componentDidMount () {
        let user = await AsyncStorage.getItem('@user');
       
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
        this.Profile();
    }
    render(){
        // console.log('user_detail', this.state.user_detail.fname);
        let user_detail = this.state.user_detail;
       
        const iconSettings = require('../../../assets/images/drawer/settings.png');
        const iconBlog = require('../../../assets/images/drawer/blog.png')
        
        let {...props} = this.props;
        let {navigation} = this.props;
        return(
            <DrawerContentScrollView {...props} style={{padding: 0,backgroundColor:"#f58837"}}>
                <TouchableOpacity onPress={() => navigation.push('Profile')}>
                    <View style={styles.avatarContainer}>
                        <Image
                        style={styles.avatar}
                        source={user_detail.image ? {uri : url + user_detail.image} : require('../../../assets/images/RNS_nerd.png')}
                        />
                        <View style={{ paddingLeft: 15 }}>
                        <Text style={styles.userName}>{user_detail.fname}</Text>
                        <Text style={{ color: '#fff' }}>{user_detail.email}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={styles.divider} />
                {this.state.drawerData.map((item, idx) => (
                    <DrawerItem
                    key={`drawer_item-${idx+1}`}
                    label={() => (
                        <View
                        style={styles.menuLabelFlex}>
                        <Image
                            style={{ width: 20, height: 20}}
                            source={item.icon}
                        />
                        <Text style={styles.menuTitle}>{item.title}</Text>
                        </View>
                    )}
                    onPress={() => navigation.push(item.name)}
                    />
                ))}
                
                <View style={styles.divider} />
                <DrawerItem
                    label={() => (
                    <View style={styles.menuLabelFlex}>
                        <Image
                        style={{ width: 20, height: 20}}
                        source={iconSettings} 
                        />
                        <Text style={styles.menuTitle}>Change Password</Text>
                    </View>
                    )}
                    onPress={() => navigation.push('ChangePasswordScreen')}
                />
                <DrawerItem
                    label={() => (
                    <View style={styles.menuLabelFlex}>
                        <Image
                        style={{ width: 20, height: 20}}
                        source={iconSettings} 
                        />
                        <Text style={styles.menuTitle}>Logout</Text>
                    </View>
                    )}
                    onPress={() => this.Logout()}
                />
                </DrawerContentScrollView>
        );
    }
}

const styles = StyleSheet.create({
    menuTitle: {
      marginLeft: 10,
      color: '#000'
    },
    menuLabelFlex: {
      display: 'flex',
      flexDirection: 'row'
    },
    userName: {
      color: '#000',
      fontSize: 18
    },
    divider: {
      borderBottomColor: '#000',
      opacity: 0.2,
      borderBottomWidth: 1,
      margin: 15,
    },
    avatar: {
      width: 40,
      height: 40,
      borderRadius: 20,
    },
    avatarContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
      margin: 20,
      marginBottom: 10
    },
  });