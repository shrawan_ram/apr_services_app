import * as React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, StatusBar } from 'react-native';
import { 
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import NavigatorView from './RootNavigation';

import AvailableInFullVersion from '../../modules/availableInFullVersion/AvailableInFullVersionViewContainer';
import HomeScreen from '../home/HomeView';
import LoginScreen from '../login/LoginView';
import PagesScreen from '../pages/PagesView';
import GridsScreen from '../grids/GridsView';
import GalleryScreen from '../gallery/GalleryView';
import FilterFormScreen from '../Mobile_Insurance/FilterForm';
import PlanListScreen from '../Mobile_Insurance/PlanList';
import AddInsuranceFormScreen from '../Mobile_Insurance/AddInsuranceForm';
import { Component } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CustomDrawerContent from './CustomeDrawer';
import HeaderLeftComponent from './HeaderLeftComponent';
import InsuranceScreen from '../Mobile_Insurance/InsuranceView';
import { colors } from '../../styles';
import ChangePasswordScreen from '../login/ChangePassword';
import RegisterScreen from '../register/RegisterView';
import OtpVerifyScreen from '../register/OtpVerifyView';
import ForgotPasswordScreen from '../register/ForgotPasswordView';
import ProfileScreen from '../profile/ProfileView';
import EditProfileScreen from '../profile/EditProfile';
import DashboardScreen from '../admin/DashboardScreen';
import StaffScreen from '../admin/staff/StaffView';
import AddStaff from '../admin/staff/AddStaff';
import StaffDetailScreen from '../admin/staff/StaffDetail';
import InsurancesScreen from '../Mobile_Insurance/InsurancesScreen';
import SettingScreen from '../admin/SettingScreen';
import { createStackNavigator } from '@react-navigation/stack';
import DrawerNavigation from './DrawerNavigation';




const Stack = createStackNavigator();



// const headerLeftComponent = (navigation) => {
//   return (
//     <TouchableOpacity
//       onPress={() => navigation.goBack()}
//       style={{
//         paddingHorizontal: 16,
//         paddingVertical: 12,
//       }}
//     >
//       <Image
//         source={require('../../../assets/images/icons/arrow-back.png')}
//         resizeMode="contain"
//         style={{
//           height: 20,
//         }}
//       />
//     </TouchableOpacity>    
//   )
// }

export default class App extends Component {
  state = {
    user: null,
    token: '',
    user_detail:'',
    loading: false
  }
  async componentDidMount () {
    let token = await AsyncStorage.getItem('@token');
    token = token !== null ? token : '';
    this.setState({token: token});

    let user = await AsyncStorage.getItem('@user');
    user = user !== null ? JSON.parse(user) : '';
    this.setState({user_detail: user});

    
}
  render(){
    // let self = this;
    let token = this.state.token;
    // console.log('user_detail', this.state.user_detail);
    let user_detail = this.state.user_detail;
    setTimeout(() => {
      this.setState({ loading: true })
    }, 3000)
    
    //  let {navigation} = this.props;
     if (this.state.loading) {
    return (
      <Stack.Navigator
        drawerStyle={{
          backgroundColor: '#3C38B1'
        }}
        drawerContent={props => <CustomDrawerContent {...props} />}
        initialRouteName={ token == '' ? "Login" : "Homes"}
      >
        {
          user_detail.role_id == '4'
          ?
          <Stack.Screen 
            name="Homes" 
            component={DrawerNavigation} 
            options={{ 
              headerShown:false,
              title:'APR Services',  
              headerTintColor: '#fff', 
              headerStyle:{  backgroundColor:'#00afef' } 
            }}
          />          
          :
          <Stack.Screen 
          name="Homes"
          component={DrawerNavigation} 
          options={{  
            headerShown:false,           
            title:'Dashboard',  
            headerTintColor: '#fff', 
            headerStyle:{  backgroundColor:'#00afef' } 
          }}
        />

        }
        <Stack.Screen 
          name="Login" 
          component={LoginScreen} 
          options={{ 
            headerShown:false, 
          }} 
        />
        <Stack.Screen 
          name="Register" 
          component={RegisterScreen} 
          options={{ 
            headerShown:false, 
          }} 
        />
        <Stack.Screen 
          name="OtpVerify" 
          component={OtpVerifyScreen} 
          options={{ 
            headerShown:false, 
          }} 
        />
        <Stack.Screen 
          name="Profile" 
          component={ProfileScreen}  
          options={({navigation}) => ({ 
            title:'Profile',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        <Stack.Screen 
          name="EditProfile" 
          component={EditProfileScreen}  
          options={({navigation}) => ({ 
            title:'Edit Profile',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        <Stack.Screen 
          name="ForgotPassword" 
          component={ForgotPasswordScreen}  
          options={({navigation}) => ({ 
            title:'Change Password',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        <Stack.Screen 
          name="ChangePasswordScreen" 
          component={ChangePasswordScreen}  
          options={({navigation}) => ({ 
            title:'Change Password',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        <Stack.Screen 
          name="FilterForm" 
          component={FilterFormScreen}  
          options={({navigation}) => ({ 
            title:'Enter Your Device Details',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            // headerLeftOnPress: () => true,
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />

        <Stack.Screen 
          name="PlanList" 
          component={PlanListScreen}  
          options={({navigation})=>({ 
            title:'Plans',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        <Stack.Screen 
          name="AddInsuranceForm" 
          component={AddInsuranceFormScreen}  
          options={({navigation})=>({ 
            title:'Enter Your Device Details',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        <Stack.Screen 
          name="InsuranceScreen" 
          component={InsuranceScreen}  
          options={({navigation})=>({ 
            title:'Insurance',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        <Stack.Screen 
          name="InsurancesScreen" 
          component={InsurancesScreen}  
          options={({navigation})=>({ 
            title:'Insurances',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />

       

        {/* Admin Route */}
        <Stack.Screen 
          name="AddStaff"
          component={AddStaff}
          options={({navigation})=>({ 
            title:'Add Staff',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        <Stack.Screen 
          name="StaffView"
          component={StaffScreen}
          options={({navigation})=>({ 
            title:'List Staff',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        <Stack.Screen 
          name="StaffDetail"
          component={StaffDetailScreen}
          options={({navigation})=>({ 
            title:'Staff Detail',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        <Stack.Screen 
          name="Setting"
          component={SettingScreen}
          options={({navigation})=>({ 
            title:'Edit Setting',
            headerLeft: () => <HeaderLeftComponent onPress={() => navigation.goBack()}/>,
            headerTintColor: '#fff', 
            headerShown:true,
            headerStyle:{  backgroundColor:'#00afef' } 
          })}
        />
        
        
      </Stack.Navigator>
    );
  } else {
    return (
      <View style={{flex:1,backgroundColor:colors.bgColor,justifyContent:'center'}}>
        <View>
          <Text style={{ color: 'black',textAlign:'center',fontSize:22 }}>Loading...</Text>
        </View>
      </View>
    )
  }
  }

}

const styles = StyleSheet.create({
  menuTitle: {
    marginLeft: 10,
    color: '#000'
  },
  menuLabelFlex: {
    display: 'flex',
    flexDirection: 'row'
  },
  userName: {
    color: '#000',
    fontSize: 18
  },
  divider: {
    borderBottomColor: '#000',
    opacity: 0.2,
    borderBottomWidth: 1,
    margin: 15,
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  avatarContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: 20,
    marginBottom: 10
  },
});
