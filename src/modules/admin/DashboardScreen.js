import AsyncStorage from "@react-native-async-storage/async-storage";
import React,{ Component } from "react";
import { ActivityIndicator, Image, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import { admin_Profile, user_Profile } from "../../Api/auth.api";
import { dashboard } from "../../Api/dashboard.api";
import { url } from "../../configs/constants.config";
import { colors } from "../../styles";


export default class DashboardScreen extends Component{
    state = {
        user_detail: '',
        dashboard:[],
        loading: false
    }

    Profile = async () => {

        let role_id = this.state.user_detail.role_id;
        // console.log('role',role_id);
        let response = '';
        if(role_id == '4'){
            response = await user_Profile();
        } else {
            response = await admin_Profile();
        }
      if(response.status){
          this.setState({user_detail: response.data.data});
              }  
      
      }

      Dashboard = async () => {

        
        let response = await dashboard();
        if(response.status){
            this.setState({dashboard: response.data});
        }  
      
      }
    async componentDidMount () {
        let token = await AsyncStorage.getItem('@token');

        let user = await AsyncStorage.getItem('@user');
        let type = await AsyncStorage.getItem('@type');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});  
        this.Profile();     
        this.Dashboard();     
       
      }

    render(){
        // console.log('dashboard',this.state.dashboard);
        let {navigation} = this.props;
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
          
        if (this.state.loading) {
        return(
            <SafeAreaView>                
                <StatusBar backgroundColor="#00afef" barStyle="light-content" />
                <TouchableOpacity onPress={() => navigation.push('Profile')}>
                    <View style={{backgroundColor:"#00afef",flexDirection:'row',paddingHorizontal:15,paddingVertical:10}}>
                        <View style={{borderRadius:50,overflow:'hidden',width:70,height:70,justifyContent:'center',backgroundColor:'#fff'}}>
                            <Image source={this.state.user_detail.image ? {uri : url + this.state.user_detail.image} : require('../../../assets/images/RNS_nerd.png')} style={styles.profile_image} />
                        </View>
                        <View style={{paddingHorizontal:15,justifyContent:'center'}}>
                            <Text style={{fontSize:22,color:'#fff'}}>Welcome !</Text>
                            <Text style={{fontSize:22,color:'#fff',fontWeight:'bold'}}>{this.state.user_detail.fname}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <ScrollView>
                    <View style={{paddingHorizontal:15,paddingVertical:10}}>
                        <View style={styles.home_products}>
                            {(() => {
                                if ( this.state.user_detail.role_id == '1'){
                                    return (
                                        <>
                                        <View style={styles.product_space}>
                                            <TouchableOpacity onPress={() => navigation.push('StaffView',{role:'Franchise'})}>
                                                <View style={styles.product_box}>
                                                    <View style={styles.product_box_bg}>
                                                        {/* <Icon type="ionicons" name="list" /> */}
                                                        <Text style={styles.data_count}>{this.state.dashboard.franchise}</Text>
                                                        <Text style={styles.box_title}>Franchise</Text>
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        </>
                                    )
                                }
                                
                                return null;
                            })()}
                            {(() => {
                                if ( this.state.user_detail.role_id == '1' || this.state.user_detail.role_id == '2' ){
                                    return (
                                        <>
                                        <View style={styles.product_space}>
                                            <TouchableOpacity onPress={() => navigation.push('StaffView',{role:'Shop'})}>
                                                <View style={styles.product_box}>
                                                    <View style={styles.product_box_bg}>
                                                        {/* <Icon type="ionicons" name="list" /> */}
                                                        <Text style={styles.data_count}>{this.state.dashboard.shop}</Text>
                                                        <Text style={styles.box_title}>Shop</Text>
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={styles.product_space}>
                                            <TouchableOpacity onPress={() => navigation.push('StaffView',{role:'Staff'})}>
                                                <View style={styles.product_box}>
                                                    <View style={styles.product_box_bg}>
                                                        {/* <Icon type="ionicons" name="list" /> */}
                                                        <Text style={styles.data_count}>{this.state.dashboard.staff}</Text>
                                                        <Text style={styles.box_title}>Staff</Text>
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        </>
                                    )
                                }
                                
                                return null;
                            })()}
                            
                            
                            <View style={styles.product_space}>
                                <TouchableOpacity onPress={() => navigation.push('InsurancesScreen')}>
                                    <View style={styles.product_box}>
                                        <View style={styles.product_box_bg}>
                                            {/* <Icon type="ionicons" name="list" /> */}
                                            <Text style={styles.data_count}>{this.state.dashboard.insurance}</Text>
                                            <Text style={styles.box_title}>Mobile Repair Services</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.product_space}>
                                <TouchableOpacity onPress={() => navigation.push('ChangePasswordScreen')}>
                                    <View style={styles.product_box}>
                                        <View style={styles.product_box_bg}>
                                            <Icon type="ionicons" name="settings" size={48} />
                                            {/* <Text style={styles.data_count}>500</Text> */}
                                            <Text style={styles.box_title}>Change Password</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            {(() => {
                                if ( this.state.user_detail.role_id == '1'){
                                    return (
                                        <>
                                        
                                        <View style={styles.product_space}>
                                            <TouchableOpacity onPress={() => navigation.push('Setting')}>
                                                <View style={styles.product_box}>
                                                    <View style={styles.product_box_bg}>
                                                        {/* <Icon type="ionicons" name="list" /> */}
                                                        <Icon type="ionicons" name="settings" size={48} />
                                                        <Text style={styles.box_title}>Setting</Text>
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        </>
                                    )
                                }
                                
                                return null;
                            })()}
                            
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    } else {
        return (
          <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
            <ActivityIndicator  color={colors.primary} />
          </View>
        )
      }
    }
}

const styles = StyleSheet.create({
    profile_image: {
        width:'100%',
        resizeMode:"center",
        height: 60,
        // borderRadius:50,        
    },
    home_products: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: -15
    },
    product_space: {
        width: '50%',
        paddingHorizontal: 15,
        marginBottom: 15
    },
    product_box_bg: {
        position: 'relative',
        backgroundColor: '#ccc',
        borderRadius: 10,
        alignItems: 'center',
        overflow: 'hidden',
        borderColor:'#f58634',
        borderWidth:1,
        padding:5,
        minHeight:104
    },
    data_count:{
        fontSize:45,
        fontWeight:'bold'
    },
    box_title:{
        fontSize:22,
        fontWeight:'bold',
        textAlign:'center',
        minHeight:42
    }
})