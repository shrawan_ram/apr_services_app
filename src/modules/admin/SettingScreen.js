import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  Modal,
  Text,
  ActivityIndicator,
} from 'react-native';

import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { ScrollBar } from 'react-native-ui-lib';
import { SafeAreaView } from 'react-native-safe-area-context';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
// import * as ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import FormData from 'form-data';
import { Button } from '../../components';
import { edit_setting, get_setting } from '../../Api/setting.api';
import { colors } from '../../styles';

export default class SettingScreen extends Component {
  
    state = {
      date              : new Date(),
      logo              :null,
      favicon           :null,
      title             :'',
      tagline           :'',
      mobile            :'',
      email             :'',
      address           :'',
      facebook          :'',
      instagram         :'',
      twitter           :'',
      pintertest        :'',
      youtube           :'',
      sms_api           :'',
      bank_name         :'',
      branch_name       :'',
      bank_ac          :'',
      bank_ifsc         :'',
      GST               :'',
      invoice_terms     :'',
      modalVisible      : false,
      field             :'logo',
      Setting           : [],
      loading           : false
    }

    getSetting = async () => {

        let response = await get_setting();
        console.log('responses', response.data.data);
        if(response.status){
                this.setState({Setting: response.data.data});
        } 
        let detail = this.state.Setting;
        console.log('detail',detail);             
        this.setState({title: detail.title});
        this.setState({tagline: detail.tagline});
        // this.setState({password: detail.password});
        this.setState({mobile: detail.mobile});
        this.setState({email: detail.email});
        this.setState({address: detail.address});
        this.setState({facebook: detail.facebook});
        this.setState({instagram: detail.instagram});
        this.setState({twitter: detail.twitter});
        this.setState({pintertest: detail.pintertest});
        this.setState({youtube: detail.youtube});
        this.setState({sms_api: detail.sms_api});
        this.setState({bank_name: detail.bank_name});
        this.setState({branch_name: detail.branch_name});
        this.setState({bank_ac: detail.bank_ac});
        this.setState({bank_ifsc: detail.bank_ifsc});
        this.setState({GST: detail.GST});
        this.setState({invoice_terms: detail.invoice_terms});
        // this.setState({logo: detail.logo});
        // this.setState({favicon: detail.favicon});
        
    }

    
    editSetting = async () => {
        let data = {
                title                   : this.state.title,
                tagline                 : this.state.tagline,
                mobile                  : this.state.mobile,
                email                   : this.state.email,
                address                 : this.state.address,
                facebook                : this.state.facebook,
                instagram               : this.state.instagram,
                twitter                 : this.state.twitter,
                pintertest              : this.state.pintertest,
                youtube                 : this.state.youtube,
                sms_api                 : this.state.sms_api,
                bank_name               : this.state.bank_name,
                branch_name             : this.state.branch_name,
                bank_ac                 : this.state.bank_ac,
                bank_ifsc               : this.state.bank_ifsc,
                GST                     : this.state.GST,
                invoice_terms           : this.state.invoice_terms,
               

        }
        data = this.createFormData(this.state.logo,this.state.favicon, data);
        
        let response = await edit_setting(data);

        // console.log("response", response);
        if (response.data.status == false) {
                alert(response.data.message);
              }

        if(response.data.status){
                // console.log('true');
                this.props.navigation.push('Homes');          
        } 
      
    
    }
    createFormData = (logo,favicon, body = {}) => {
        const data = new FormData();
        
        
        if(logo) {
                data.append('logo', {
                    name: logo.fileName,
                    type: logo.type,
                    uri: Platform.OS === 'ios' ? logo.uri.replace('file://', '') : logo.uri,
                });
        }
        if(favicon) {
                data.append('favicon', {
                    name: favicon.fileName,
                    type: favicon.type,
                    uri: Platform.OS === 'ios' ? favicon.uri.replace('file://', '') : favicon.uri,
                });
        }
        
        
        Object.keys(body).forEach((key) => {
            data.append(key, body[key]);
        });
        
        return data;
    }    
   

    handleChoosePhoto (type = 'camera', field = 'logo') {
            console.log('field',this.state.field);
        let self = this
        this.setState({modalVisible: false})
        const options = {
                noData: true,
        };
        type === 'camera' ? 
                launchCamera(options, (response) => {

                        console.log('res',response);
                        if (response.assets[0].uri) {
                                self.setState({ [this.state.field]: response.assets[0] });                                
                        }
                })
        : launchImageLibrary(options, (response) => {
                if (response.assets[0].uri) {
                        self.setState({ [this.state.field]: response.assets[0] });
                }
        });
    }

    

  
    async componentDidMount () {
        let user = await AsyncStorage.getItem('@user');
       
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
        // this.setState({name: this.state.user_detail.fname});
        // this.setState({mobile: this.state.user_detail.mobile});
        // this.setState({email: this.state.user_detail.email});
        // this.setState({price: route.params.price});

            
            this.getSetting();
    }

  render(){
    const { width } = Dimensions.get('window');
    let {navigation, route} = this.props;
    setTimeout(() => {
        this.setState({ loading: true })
      }, 2000)
      
    if (this.state.loading) {
    return (
      <SafeAreaView >
        <ScrollView>
        

            <View style={styles.container}>
                    
              {/* <Text style={styles.device_detail}>Enter Device Details</Text> */}
              
             
            <Text style={styles.label}>Title</Text>
            <TextInput style = {[styles.input]}
                    underlineColorAndroid = "transparent"
                    placeholder = "Enter title"
                    placeholderTextColor = "#aaa"
                    autoCapitalize = "none"
                    
                    value={this.state.title}  
                    onChangeText={title => this.setState({ title })}
            />
            
            <Text style={styles.label}>Tagline</Text>
            <TextInput style = {[styles.input]}
                    underlineColorAndroid = "transparent"
                    placeholder = "Enter tagline"
                    placeholderTextColor = "#aaa"
                    autoCapitalize = "none"
                    
                    value={this.state.tagline}  
                    onChangeText={tagline => this.setState({ tagline })}
            />
                     
              
              
              <Text style={styles.label}>Email</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter email"
                      placeholderTextColor = "#aaa"
                //       
                      autoCapitalize = "none"
                      value={this.state.email}  
                      onChangeText={email => this.setState({ email })}
              />
              <Text style={styles.label}>Mobile Number</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter mobile no."
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      
                      value={this.state.mobile}  
                      onChangeText={mobile => this.setState({ mobile })}
              />
              
              <Text style={styles.label}>Bank Account No.</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter a/c no."
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      
                      value={this.state.bank_ac}  
                      onChangeText={bank_ac => this.setState({ bank_ac })}
              />
              {/* <Text style={styles.label}>Acccount Holder</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter a/c holder"
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      
                      value={this.state.acc_holder}  
                      onChangeText={acc_holder => this.setState({ acc_holder })}
              /> */}
              <Text style={styles.label}>Ifsc Code</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter ifsc"
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      
                      value={this.state.bank_ifsc}  
                      onChangeText={bank_ifsc => this.setState({ bank_ifsc })}
              />
              <Text style={styles.label}>Bank Name</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter bank name"
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      
                      value={this.state.bank_name}  
                      onChangeText={bank_name => this.setState({ bank_name })}
              />
              <Text style={styles.label}>Branch Name</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter branch name"
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      
                      value={this.state.branch_name}  
                      onChangeText={branch_name => this.setState({ branch_name })}
              />
              <Text style={styles.label}>Address</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter address"
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      value={this.state.address}  
                      onChangeText={address => this.setState({ address })}
              />
              
              <View style={{flexDirection:'row',marginBottom:5}}>
                <Text style={styles.choose_btn_left}  onPress={() =>this.setState({modalVisible: true, field: 'logo'})} >Choose Logo</Text>
                <Text style={styles.choose_btn_right}  onPress={() =>this.setState({modalVisible: true, field: 'favicon'})} >Choose Favicon</Text>
              </View>
              
                <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        this.setState({modalVisible: false});
                        }}
                >
                        <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                                <TouchableOpacity onPress={()=>this.handleChoosePhoto('camera', this.state.field)}>
                                        <Text style={styles.modalText}>Take Photo...</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>this.handleChoosePhoto('library')}>
                                        <Text style={styles.modalText}>Choose From library...</Text>
                                </TouchableOpacity>
                        
                        <TouchableOpacity
                        onPress={() => this.setState({modalVisible: false})}
                        >
                        <Text style={styles.textStyle}>Cancel</Text>
                        </TouchableOpacity>
                        </View>
                        </View>
                </Modal>
                <Text style={styles.label}>Facebook</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter facebook"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        value={this.state.facebook}  
                        onChangeText={facebook => this.setState({ facebook })}
                />
                <Text style={styles.label}>Instagram</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter instagram"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        value={this.state.instagram}  
                        onChangeText={instagram => this.setState({ instagram })}
                />
                <Text style={styles.label}>Twitter</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter twitter"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        value={this.state.twitter}  
                        onChangeText={twitter => this.setState({ twitter })}
                />
                <Text style={styles.label}>Youtube</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter youtube"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        value={this.state.youtube}  
                        onChangeText={youtube => this.setState({ youtube })}
                />
                <Text style={styles.label}>Pintertest</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter pintertest"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        value={this.state.pintertest}  
                        onChangeText={pintertest => this.setState({ pintertest })}
                />
                <Text style={styles.label}>Sms api</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter sms_api"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        value={this.state.sms_api}  
                        onChangeText={sms_api => this.setState({ sms_api })}
                />
                <Text style={styles.label}>GST</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter GST"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        value={this.state.GST}  
                        onChangeText={GST => this.setState({ GST })}
                />
                <Text style={styles.label}>invoice_terms</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter invoice_terms"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        value={this.state.invoice_terms}  
                        onChangeText={invoice_terms => this.setState({ invoice_terms })}
                />
                <Button
                    style={[styles.demoButton],{marginTop:8}}
                    primary
                    caption='Edit Setting'
                    onPress={() => this.editSetting()}
                />
              
                
        </View>
        </ScrollView>
        
      </SafeAreaView>
    );
}
else {
        return (
                <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                  <ActivityIndicator  color={colors.primary} />
                </View>
              )    
}
  }
}

const styles = StyleSheet.create({
    input: {
        // margin: 15,
        marginVertical:10,
        height: 40,
        color:'#000',
        borderColor: '#00afef', 
        borderWidth: 1,
        width:'100%',
        backgroundColor:'#fff',
        borderRadius:5
     },
     container: {
        // flex: 1,
        // alignItems: 'center',
        // justifyContent: 'space-around',
        paddingHorizontal:15,
        paddingVertical:15
      },
      centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        backgroundColor:'rgba(52, 52, 52, .3)',
        
      },
      modalView: {
        margin: 0,
        width:300,
        backgroundColor: "white",
        borderRadius: 5,
        padding: 35,
        // alignItems: "left",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "#000",
        fontWeight: "bold",
        textAlign: "right",
        fontSize:17,
        textTransform:'uppercase'
      },
      modalText: {
        marginBottom: 15,
        paddingVertical:8,
        fontSize:17
      },
      choose_btn_left :{
        flex:.6,
        marginRight:2,
        marginBottom:2,
        backgroundColor:'#aaa',
        textAlign:'center',
        borderRadius:5,
        paddingVertical:8
      },
      choose_btn_right :{
        flex:.6,
        marginBottom:2,
        backgroundColor:'#aaa',
        textAlign:'center',
        borderRadius:5,
        paddingVertical:8
      }

})