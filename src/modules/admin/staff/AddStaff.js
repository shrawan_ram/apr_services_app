import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  Modal,
  Text,
  ActivityIndicator,
} from 'react-native';

import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { styles } from './style';
import { ScrollBar } from 'react-native-ui-lib';
import { SafeAreaView } from 'react-native-safe-area-context';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
// import * as ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import FormData from 'form-data';
import { Button } from '../../../components';
import { add_staff, edit_staff, staff_Detail } from '../../../Api/staff.api';
import { colors } from '../../../styles';

export default class AddStaff extends Component {
  
    state = {
      date              : new Date(),
      aadhar_front      :null,
      aadhar_back       :null,
      pan_img           :null,
      bank_statement    :null,
      id                :'',
      user_detail       :'',
      shop_name         :'',
      fname             :'',
      lname             :'',
      mobile            :'',
      alternative_mobile_no   :'',
      whatsapp_no       :'',
      password          :'',
      email             :'',
      aadhar_no         :'',
      acc_no            :'',
      acc_holder        :'',
      ifsc_code         :'',
      bank_name         :'',
      branch_name       :'',
      address           :'',
      city              :'',
      state             :'',
      pancard           :'',
      pincode           :'',
      modalVisible      : false,
      field             :'aadhar_front',
       loading          :false,
      Staff_Detail      : [],
    }

    getStaff_Detail = async (id) => {

        let response = await staff_Detail(id);
        console.log('responses', response.data.data);
        if(response.status){
                this.setState({Staff_Detail: response.data.data});
        } 
        let detail = this.state.Staff_Detail;
        console.log('detail',detail);             
        this.setState({fname: detail.fname});
        this.setState({lname: detail.lname});
        // this.setState({password: detail.password});
        this.setState({mobile: detail.mobile});
        this.setState({alternative_mobile_no: detail.alternative_mobile_no});
        this.setState({whatsapp_no: detail.alternative_whatsapp_no});
        this.setState({email: detail.email});
        this.setState({aadhar_no: detail.aadharcard});
        this.setState({pancard: detail.pancard});
        this.setState({acc_no: detail.acc_no});
        this.setState({acc_holder: detail.acc_holder});
        this.setState({ifsc_code: detail.ifsc_code});
        this.setState({bank_name: detail.bank_name});
        this.setState({branch_name: detail.branch_name});
        this.setState({address: detail.address});
        this.setState({city: detail.city});
        this.setState({state: detail.state});
        this.setState({pincode: detail.pincode});
        // this.setState({aadhar_front: detail.aadhar_front});
        // this.setState({aadhar_back: detail.aadhar_back});
        // this.setState({pan_imag: detail.pan_imag});
        // this.setState({bank_statement: detail.bank_statement});
        this.setState({shop_name: detail.shop_name});
        this.setState({id: detail.id});
        
    }

    addStaff = async () => {
        let {route} = this.props;
        let data = {
                shop_name               : this.state.shop_name,
                fname                   : this.state.fname,
                lname                   : this.state.lname,
                mobile                  : this.state.mobile,
                alternative_mobile_no   : this.state.alternative_mobile_no,
                alternative_whatsapp_no : this.state.whatsapp_no,
                password                : this.state.password,
                email                   : this.state.email,
                aadharcard              : this.state.aadhar_no,
                pancard                 : this.state.pancard,
                address                 : this.state.address,
                acc_no                  : this.state.acc_no,
                acc_holder              : this.state.acc_holder,
                ifsc_code               : this.state.ifsc_code,
                bank_name               : this.state.bank_name,
                branch_name             : this.state.branch_name,
                city                    : this.state.city,
                state                   : this.state.state,
                pincode                 : this.state.pincode,
                role                    : route.params.role,
               

        }
        data = this.createFormData(this.state.aadhar_front,this.state.aadhar_back,this.state.pan_img,this.state.bank_statement, data);
        
        let response = await add_staff(data);

        console.log("response", response);
        if (response.status == false) {
                alert(response.error.message);
              }
        if(response.status){
                // console.log('true');
                this.props.navigation.push('StaffView',route.params);          
        } 
      
    
    }
    editStaff = async () => {
        let {route} = this.props;
        let data = {
                id                      : this.state.id,
                shop_name               : this.state.shop_name,
                fname                   : this.state.fname,
                lname                   : this.state.lname,
                mobile                  : this.state.mobile,
                alternative_mobile_no   : this.state.alternative_mobile_no,
                alternative_whatsapp_no : this.state.whatsapp_no,
                password                : this.state.password,
                email                   : this.state.email,
                aadharcard              : this.state.aadhar_no,
                pancard                 : this.state.pancard,
                address                 : this.state.address,
                acc_no                  : this.state.acc_no,
                acc_holder              : this.state.acc_holder,
                ifsc_code               : this.state.ifsc_code,
                bank_name               : this.state.bank_name,
                branch_name             : this.state.branch_name,
                city                    : this.state.city,
                state                   : this.state.state,
                pincode                 : this.state.pincode,
                role                    : route.params.role,
               

        }
        data = this.createFormData(this.state.aadhar_front,this.state.aadhar_back,this.state.pan_img,this.state.bank_statement, data);
        
        let response = await edit_staff(data);

        // console.log("edit response", response);
        if (response.data.status == false) {
                alert(response.data.message);
              }

        if(response.data.status){
                // console.log('true');
                this.props.navigation.push('StaffView',route.params);          
        } 
      
    
    }
    createFormData = (aadhar_front,aadhar_back,pan_img,bank_statement, body = {}) => {
        const data = new FormData();
        
        
        if(aadhar_front) {
                data.append('aadhar_front', {
                    name: aadhar_front.fileName,
                    type: aadhar_front.type,
                    uri: Platform.OS === 'ios' ? aadhar_front.uri.replace('file://', '') : aadhar_front.uri,
                });
        }
        if(aadhar_back) {
                data.append('aadhar_back', {
                    name: aadhar_back.fileName,
                    type: aadhar_back.type,
                    uri: Platform.OS === 'ios' ? aadhar_back.uri.replace('file://', '') : aadhar_back.uri,
                });
        }
        if(pan_img) {
                data.append('pan_img', {
                    name: pan_img.fileName,
                    type: pan_img.type,
                    uri: Platform.OS === 'ios' ? pan_img.uri.replace('file://', '') : pan_img.uri,
                });
        }
        if(bank_statement) {
                data.append('bank_statement', {
                    name: bank_statement.fileName,
                    type: bank_statement.type,
                    uri: Platform.OS === 'ios' ? bank_statement.uri.replace('file://', '') : bank_statement.uri,
                });
        }
        
        Object.keys(body).forEach((key) => {
            data.append(key, body[key]);
        });
        
        return data;
    }    
   

    handleChoosePhoto (type = 'camera', field = 'photo') {
            console.log('field',this.state.field);
        let self = this
        this.setState({modalVisible: false})
        const options = {
                noData: true,
        };
        type === 'camera' ? 
                launchCamera(options, (response) => {

                        console.log('res',response);
                        if (response.assets[0].uri) {
                                self.setState({ [this.state.field]: response.assets[0] });                                
                        }
                })
        : launchImageLibrary(options, (response) => {
                if (response.assets[0].uri) {
                        self.setState({ [this.state.field]: response.assets[0] });
                }
        });
    }

    

  
    async componentDidMount () {
        let user = await AsyncStorage.getItem('@user');
       
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
        // this.setState({name: this.state.user_detail.fname});
        // this.setState({mobile: this.state.user_detail.mobile});
        // this.setState({email: this.state.user_detail.email});
        // this.setState({price: route.params.price});

        let {route} = this.props;
            let id = route.params.item.id;
            
            this.getStaff_Detail(id);
    }

  render(){
        const { width } = Dimensions.get('window');
        let {navigation, route} = this.props;
        // console.log('staff_detail',this.state.Staff_Detail);
        setTimeout(() => {
                this.setState({ loading: true })
        }, 2000)
        
        if (this.state.loading) {
        return (
        <SafeAreaView >
                <ScrollView>
                

                <View style={styles.container}>
                        
                {/* <Text style={styles.device_detail}>Enter Device Details</Text> */}
                {(() => {
                        if (route.params.role == 'Shop'){
                                return (<>
                                        <Text style={styles.label}>Shop Name</Text>
                                        <TextInput style = {[styles.input]}
                                                underlineColorAndroid = "transparent"
                                                placeholder = "Enter shop name."
                                                placeholderTextColor = "#aaa"
                                                autoCapitalize = "none"
                                                
                                                value={this.state.shop_name}  
                                                onChangeText={shop_name => this.setState({ shop_name })}
                                        />
                                        </>
                                )
                        }
                        
                        return null;
                        })()}
                <View style={{flexDirection:'row'}}>
                        <View style={{flex:.5,paddingRight:5}}>
                        <Text style={styles.label}>First Name</Text>
                                <TextInput style = {[styles.input]}
                                        underlineColorAndroid = "transparent"
                                        placeholder = "Enter first name"
                                        placeholderTextColor = "#aaa"
                                        autoCapitalize = "none"
                                        
                                        value={this.state.fname}  
                                        onChangeText={fname => this.setState({ fname })}
                                />
                        </View>
                        <View style={{flex:.5}}>
                        <Text style={styles.label}>Last Name</Text>
                                <TextInput style = {[styles.input]}
                                        underlineColorAndroid = "transparent"
                                        placeholder = "Enter Last Name"
                                        placeholderTextColor = "#aaa"
                                        autoCapitalize = "none"
                                        
                                        value={this.state.lname}  
                                        onChangeText={lname => this.setState({ lname })}
                                />
                        </View>
                </View>
                
                
                <Text style={styles.label}>Email</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter email"
                        placeholderTextColor = "#aaa"
                        //       
                        autoCapitalize = "none"
                        value={this.state.email}  
                        onChangeText={email => this.setState({ email })}
                />
                <Text style={styles.label}>Mobile Number</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter mobile no."
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.mobile}  
                        onChangeText={mobile => this.setState({ mobile })}
                />
                <Text style={styles.label}>alternative Mobile Number</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter alt mobile no."
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.alternative_mobile_no}  
                        onChangeText={alternative_mobile_no => this.setState({ alternative_mobile_no })}
                />
                <Text style={styles.label}>Password</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter password"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.password}  
                        onChangeText={password => this.setState({ password })}
                />
                <Text style={styles.label}>Pan No.</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter pancard no."
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.pancard}  
                        onChangeText={pancard => this.setState({ pancard })}
                />
                <Text style={styles.label}>Aadhar No.</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter aadhar no."
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        value={this.state.aadhar_no}  
                        onChangeText={aadhar_no => this.setState({ aadhar_no })}
                />
                <Text style={styles.label}>Bank Account No.</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter a/c no."
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.acc_no}  
                        onChangeText={acc_no => this.setState({ acc_no })}
                />
                <Text style={styles.label}>Acccount Holder</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter a/c holder"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.acc_holder}  
                        onChangeText={acc_holder => this.setState({ acc_holder })}
                />
                <Text style={styles.label}>Ifsc Code</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter ifsc"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.ifsc_code}  
                        onChangeText={ifsc_code => this.setState({ ifsc_code })}
                />
                <Text style={styles.label}>Bank Name</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter bank name"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.bank_name}  
                        onChangeText={bank_name => this.setState({ bank_name })}
                />
                <Text style={styles.label}>Branch Name</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter branch name"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.branch_name}  
                        onChangeText={branch_name => this.setState({ branch_name })}
                />
                <Text style={styles.label}>Address</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter address"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        value={this.state.address}  
                        onChangeText={address => this.setState({ address })}
                />
                <Text style={styles.label}>City</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter city"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.city}  
                        onChangeText={city => this.setState({ city })}
                /> 
                <Text style={styles.label}>State</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter state"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.state}  
                        onChangeText={state => this.setState({ state })}
                />  
                <Text style={styles.label}>Pincode</Text>
                <TextInput style = {[styles.input]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter pincode"
                        placeholderTextColor = "#aaa"
                        autoCapitalize = "none"
                        
                        value={this.state.pincode}  
                        onChangeText={pincode => this.setState({ pincode })}
                />    
                <View style={{flexDirection:'row'}}>
                        <Text style={styles.choose_btn_left}  onPress={() =>this.setState({modalVisible: true, field: 'aadhar_front'})} >Choose Aadhar Front</Text>
                        <Text style={styles.choose_btn_right}  onPress={() =>this.setState({modalVisible: true, field: 'aadhar_back'})} >Choose Aadhar Back</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                        <Text style={styles.choose_btn_left}  onPress={() =>this.setState({modalVisible: true, field: 'pan_img'})} >Choose Pancard</Text>
                        <Text style={styles.choose_btn_right}  onPress={() =>this.setState({modalVisible: true, field: 'bank_statement'})} >Choose Bank statement</Text>
                </View>
                        <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.modalVisible}
                                onRequestClose={() => {
                                Alert.alert("Modal has been closed.");
                                this.setState({modalVisible: false});
                                }}
                        >
                                <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                        <TouchableOpacity onPress={()=>this.handleChoosePhoto('camera', this.state.field)}>
                                                <Text style={styles.modalText}>Take Photo...</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>this.handleChoosePhoto('library')}>
                                                <Text style={styles.modalText}>Choose From library...</Text>
                                        </TouchableOpacity>
                                
                                <TouchableOpacity
                                onPress={() => this.setState({modalVisible: false})}
                                >
                                <Text style={styles.textStyle}>Cancel</Text>
                                </TouchableOpacity>
                                </View>
                                </View>
                        </Modal>
                        {(() => {
                        if (route.params.item){
                                return (
                                        <Button
                                        style={[styles.demoButton],{marginTop:8}}
                                        primary
                                        caption={'Edit '+route.params.role}
                                        onPress={() => this.editStaff()}
                                        />
                                )
                        } else {
                                return(
                                        <Button
                                        style={[styles.demoButton],{marginTop:8}}
                                        primary
                                        caption={'ADD '+route.params.role}
                                        onPress={() => this.addStaff()}
                                        />
                                )
                        }
                        
                        return null;
                        })()}
                
                        
                </View>
                </ScrollView>
                
        </SafeAreaView>
        );
                }
                else {
                        return (
                                <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                                  <ActivityIndicator  color={colors.primary} />
                                </View>
                              )    
                }
  }
}


