import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React, { Component } from "react";
import { ActivityIndicator, Button, FlatList, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-ionicons";
import { DataTable } from "react-native-paper";
import RBSheet from "react-native-raw-bottom-sheet";
import { Staff_list } from "../../../Api/staff.api";
import { colors } from "../../../styles";
import { styles } from './style';

export default class StaffScreen extends Component{
    
        state = {
          Staff_list    :[],
          bottom_sheet  :[
              {
                  name : "Edit"
              },
              {
                    name : "Delete"
                },
          ],
          user_detail   :'',
          loading       : false
        }

        getStaff = async (Role) => {

            let response = await Staff_list(Role);
            console.log('response', response.data.data);
            if(response.status){
              this.setState({Staff_list: response.data.data});
            }              
            
        }
        async componentDidMount() {
            
            let user = await AsyncStorage.getItem('@user');
            user = user !== null ? JSON.parse(user) : '';
            this.setState({user_detail: user});
            
            let {route} = this.props;
            let Role = route.params.role;
            
            this.getStaff(Role);

            
        }
      
    render(){
        let {navigation,route} = this.props;
        // console.log('user',this.state.user_detail);
        setTimeout(() => {
            this.setState({ loading: true })
          }, 1000)
      
          if (this.state.loading) {
        return(
            <SafeAreaView style={{position:'relative',flex:1}}>
                
                <ScrollView>
                    <View style={{padding:10}}>                        
                        <View>
                        
                            {
                            this.state.Staff_list.map((item, index) => {
                                return(
                                <TouchableOpacity onPress={() => this[RBSheet + index].open()}>
                                    <View style={styles.order_history_content} key={index}>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{flex:.9}}>
                                            <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>Name :</Text> {item.fname} {item.lname}</Text>                      
                                        </View>
                                        <View style={{flex:.1,alignItems:'flex-end'}}>
                                            {/* <Text style={{textAlign:'right',fontSize:17}}>
                                                { moment(item.created_at).format('MMM d, YYYY') }
                                                </Text> */}
                                            <TouchableOpacity onPress={() => this[RBSheet + index].open()}>
                                                <Icon type="ionicons" name="more" size={20} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{flex:.9}}>
                                            <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>Mobile :</Text> {item.mobile}</Text>
                                        </View>
                                        
                                    </View>
                                    {
                                        item.role_id == '3' ?
                                        <View style={{flexDirection:'row'}}>
                                            <View style={{flex:.9}}>
                                                <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>Shop Name :</Text> {item.shop_name}</Text>                              
                                            </View>
                                        </View>
                                        :
                                        <></>
                                    }
                                    <RBSheet
                                        ref={ref => {
                                            this[RBSheet + index] = ref;
                                        }}
                                        draggableIcon={true}
                                        closeOnDragDown={true}
                                        height={200}
                                        customStyles={{
                                            container: {
                                            //   justifyContent: "center",
                                            //   alignItems: "center",
                                              borderTopLeftRadius:20,
                                              borderTopRightRadius:20,
                                            }
                                          }}
                                    >
                                        <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                            <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                        </View>
                                        
                                        {(() => {
                                            if(this.state.user_detail.role_id == '1'){
                                            if (route.params.role == 'Franchise'){
                                                return (<>
                                                        <TouchableOpacity onPress={() => navigation.push('AddStaff',{'role':route.params.role,item})}>
                                                            <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,padding:15}}>
                                                                <Text style={{fontSize:18}}>Edit</Text>                                            
                                                            </View>
                                                        </TouchableOpacity>
                                                        </>
                                                )
                                            }
                                        }
                                            
                                            return null;
                                        })()}
                                        {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,padding:15}}>
                                            <Text style={{fontSize:18}}>Delete</Text>                                            
                                        </View> */}
                                        <TouchableOpacity onPress={() => navigation.push('StaffDetail',item)}>
                                            <View style={{padding:15}}>
                                                <Text style={{fontSize:18}}>View Detail</Text>                                            
                                            </View>
                                        </TouchableOpacity>
                                    </RBSheet>
                                    </View>
                                 </TouchableOpacity>
                                );
                            })
                            }
                            </View>
                            {/* <Button title="OPEN BOTTOM SHEET" onPress={() => this.RBSheet.open()} /> */}
        {/* <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={300}
          openDuration={250}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center"
            }
          }}
        >
          <YourOwnComponent />
        </RBSheet> */}

        
                    </View>                    
                </ScrollView>
                {(() => {
                    if ( this.state.user_detail.role_id == '1' && route.params.role == 'Franchise'){
                        return (
                            <View style={styles.add_address_btn_position}>
                                <TouchableOpacity onPress={() => navigation.push('AddStaff',route.params)}>
                                    <View style={styles.add_address_btn}>
                                        <Icon type="ionicons" color={colors.white} name="add" />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    }
                    
                    return null;
                })()}

                {(() => {
                    if ( this.state.user_detail.role_id == '2'){
                        return (
                            <View style={styles.add_address_btn_position}>
                                <TouchableOpacity onPress={() => navigation.push('AddStaff',route.params)}>
                                    <View style={styles.add_address_btn}>
                                        <Icon type="ionicons" color={colors.white} name="add" />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    }
                    
                    return null;
                })()}
                
            </SafeAreaView>
        );
    }
    else {
            return (
                    <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                      <ActivityIndicator  color={colors.primary} />
                    </View>
                  )    
    }
    }
}