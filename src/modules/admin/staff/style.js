import { StyleSheet } from "react-native";
import { colors } from "../../../styles";

export const styles = StyleSheet.create({
    order_history_content: {
        marginVertical:5,
        backgroundColor: colors.white,
        borderRadius:5,
        padding:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        
        elevation: 3,
        },
        order_id: {
        fontSize:17,
        // fontWeight:'bold'
        color:'#000'
        },
        order_date: {
        // fontSize:27,
        // fontWeight:'bold',
        fontSize:17,
        // textAlign:'right'
        },
        product_name: {
        fontSize:17,
        fontWeight: 'bold',
        // color:colors.darkgrey
        },
        product_price: {
        // fontSize:17,
        // fontWeight: 'bold'
        textAlign:'right',
        color:colors.darkgrey
        },
        order_id_detail: {
            fontSize:17,
            // fontWeight:'bold'
            paddingVertical:8,
            color:'#000'
            },
        doc_image: {
            width:'99%',
            resizeMode:'contain',
            height:158
        },

        add_mobile_inc_position:{
            position:'absolute',
            top:'60%',
            right:'10%',
            
            backgroundColor:'red'
          },
          add_address_btn_position: {
            position:'absolute',
            bottom:'10%',
            right:'5%',
            zIndex:10
          },
          add_address_btn: {
            backgroundColor: '#00afef',
            // alignItems:'center',
            borderRadius: 50,
            width: 60,
            height: 60,
            justifyContent:'center',
            alignItems:'center',
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 3,
            },
            shadowOpacity: 0.29,
            shadowRadius: 4.65,
            
            elevation: 7,
            
          },
          input: {
            // margin: 15,
            marginVertical:10,
            height: 40,
            color:'#000',
            borderColor: '#00afef', 
            borderWidth: 1,
            width:'100%',
            backgroundColor:'#fff',
            borderRadius:5
         },
         container: {
            // flex: 1,
            // alignItems: 'center',
            // justifyContent: 'space-around',
            paddingHorizontal:15,
            paddingVertical:15
          },
          centeredView: {
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 0,
            backgroundColor:'rgba(52, 52, 52, .3)',
            
          },
          modalView: {
            margin: 0,
            width:300,
            backgroundColor: "white",
            borderRadius: 5,
            padding: 35,
            // alignItems: "left",
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 4,
            elevation: 5
          },
          button: {
            borderRadius: 20,
            padding: 10,
            elevation: 2
          },
          buttonOpen: {
            backgroundColor: "#F194FF",
          },
          buttonClose: {
            backgroundColor: "#2196F3",
          },
          textStyle: {
            color: "#000",
            fontWeight: "bold",
            textAlign: "right",
            fontSize:17,
            textTransform:'uppercase'
          },
          modalText: {
            marginBottom: 15,
            paddingVertical:8,
            fontSize:17
          },
          choose_btn_left :{
            flex:.6,
            marginRight:2,
            marginBottom:2,
            backgroundColor:'#aaa',
            textAlign:'center',
            borderRadius:5,
            paddingVertical:8
          },
          choose_btn_right :{
            flex:.6,
            marginBottom:2,
            backgroundColor:'#aaa',
            textAlign:'center',
            borderRadius:5,
            paddingVertical:8
          }

  });