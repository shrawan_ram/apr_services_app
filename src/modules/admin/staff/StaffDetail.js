import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  Alert,
  Text,
  ActivityIndicator,
} from 'react-native';

import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { styles } from './style';
import moment from 'moment';
import { url } from '../../../configs/constants.config';
import { colors } from '../../../styles';

export default class StaffDetailScreen extends Component {
  
    state = {
      loading   :false
    }
  
         

      async componentDidMount () {
        
      }

  render(){
    const images = [
      {
          source: {
              uri: 'https://cdn.pixabay.com/photo/2017/08/17/10/47/paris-2650808_960_720.jpg',
          },
          title: 'Paris',
          width: 806,
          height: 720,
      },
  ];

    const { width } = Dimensions.get('window');
    let {navigation, route} = this.props;
    let image_url = url + 'images/admin/aadhar/';
    let item = route.params;
    console.log('url', image_url + item.pan_img);
    setTimeout(() => {
      this.setState({ loading: true })
    }, 1000)

    if (this.state.loading) {
    return (
      <ScrollView >
        <StatusBar backgroundColor="#00afef" barStyle="light-content" />        
        <View style={styles.container}>        
          <View style={styles.order_history_content} key={item.id}>
            <View style={{position:'absolute',right:5,top:50}}>
              <Image source={item.photo ? {uri: url +'images/admin/photo/' + item.photo} : require('../../../../assets/images/default.png')} style={{width:100,height:100,resizeMode:'contain',borderWidth:1,borderColor:'#000',borderRadius:5}} />
            </View>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.8}}>
                    <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Name :</Text> {item.fname} {item.lname}</Text>                      
                </View>
                <View style={{flex:.4}}>
                    <Text style={{textAlign:'right',fontSize:17, fontWeight:'bold', paddingVertical:8}}>
                      { moment(item.created_at).format('MMM d, YYYY') }
                      </Text>
                </View>
            </View>
            {/* <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Name :</Text> {item.fname} {item.lname}</Text> */}
            {
               item.role_id == '3' ?
                <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Shop Name :</Text> {item.shop_name}</Text>
                :
                <></>
            }


            
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Mobile :</Text> {item.mobile} {item.alternative_mobile_no ? ', '+item.alternative_mobile_no : ''}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Whatsapp No. :</Text> {item.alternative_whatsapp_no}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Email :</Text> {item.email}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Aadhar No. :</Text> {item.aadharcard}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Pan No. :</Text> {item.pancard}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Address :</Text> {item.address}{item.city ? ', '+item.city : ''}{item.state ? ', '+item.state : ''}{item.pincode ? ', '+item.pincode : ''}</Text>

            <Text style={{fontWeight:'bold',fontSize:25}}>Bank Detail :</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Bank Name :</Text> {item.bank_name}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>A/C Holder :</Text> {item.acc_holder}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>A/C No. :</Text> {item.acc_no}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Branch Name :</Text> {item.branch_name}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Ifsc Code:</Text> {item.ifsc_code}</Text>

            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Aadhar Card Images :</Text></Text>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.6}}>
                  <Image source={ item.aadhar_front ? {uri: url +'images/admin/aadhar/' + item.aadhar_front} : require('../../../../assets/images/default.png')} style={styles.doc_image} />                      
                </View>
                <View style={{flex:.6}}>
                  <Image source={ item.aadhar_back ? {uri: url +'images/admin/aadhar/' + item.aadhar_back} : require('../../../../assets/images/default.png')} style={styles.doc_image} />
                </View>
            </View>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.6}}>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Pan Card :</Text></Text>
                  <Image source={ item.pan_img ? {uri: url +'images/admin/pan_card/' + item.pan_img} : require('../../../../assets/images/default.png')} style={styles.doc_image} />                      
                </View>
                <View style={{flex:.6}}>
                <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Bank Statement :</Text></Text>
                  <Image source={ item.bank_statement ? {uri: url +'images/admin/bank_statement/' + item.bank_statement} : require('../../../../assets/images/default.png')} style={styles.doc_image} />
                </View>
            </View>
            {/* <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Invoice Image :</Text></Text>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.6}}>
                  <Image source={ item.invoice ? {uri: url + item.invoice} : require('../../../../assets/images/default.png')} style={styles.doc_image} />                      
                </View>
                <View style={{flex:.6}}></View>     
            </View> */}
            
            
          </View>
        </View>        
      </ScrollView>
    );
  }
  else {
          return (
                  <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                    <ActivityIndicator  color={colors.primary} />
                  </View>
                )    
  }
  }
}


