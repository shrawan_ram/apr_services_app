import React,{ Component } from 'react';
import SplashScreen from 'react-native-splash-screen';

import Navigator from './navigation/Navigator';

export default class AppView extends Component {
  async componentDidMount(){
    SplashScreen.hide();
  }
  render(){
    return(
      <Navigator onNavigationStateChange={() => {}} uriPrefix="/app" />
    );
  }
}
