// import React, { Component } from 'react';
// import {
//   StyleSheet,
//   View,
//   TouchableOpacity,
//   ImageBackground,
//   Image,
//   Dimensions,
//   StatusBar,
// } from 'react-native';

// import { fonts, colors } from '../../styles';
// import { Text } from '../../components/StyledText';
// import { TextInput } from 'react-native-gesture-handler';
// import DatePicker from 'react-native-datepicker';
// import { Button, RadioGroup, Dropdown } from '../../components';
// import Carousel, { Pagination } from 'react-native-snap-carousel';

// export default class FormScreen extends Component {
  
//     state = {
//       images: [
//         {
//         image: require('../../../assets/images/1628316946New_Project.jpg')
//       },
//       {
//         image: require('../../../assets/images/1628317430New_Project(1).jpg')
//       },
//       {
//         image: require('../../../assets/images/1628319038New_Project(2).jpg')
//       }
//       ],
//       date : new Date(),
//     }
  
//   // const rnsUrl = 'https://reactnativestarter.com';
//   // const handleClick = () => {
//   //   Linking.canOpenURL(rnsUrl).then(supported => {
//   //     if (supported) {
//   //       Linking.openURL(rnsUrl);
//   //     } else {
//   //       console.log(`Don't know how to open URI: ${rnsUrl}`);
//   //     }
//   //   });
//   // };

//   render(){
//     const { width } = Dimensions.get('window');

//     return (
//       <View >
//         <StatusBar backgroundColor="#00afef" barStyle="light-content"
//   />
//         <View>
//           {/* <Image style={styles.image} source={require('../../../assets/images/avatar.png')} /> */}
//           <View style={{ height: 156 }}>
//             <View style={{flex: 1}}>
//               <Carousel
//                           ref={this.carouselRef}
//                           layout='default'
//                           data={this.state.images}
//                           sliderWidth={width -1}
//                           itemWidth={width -1}
//                           renderItem={({ item, index }) => (
                            
//                               <Image
//                                   key={index}
//                                   style={{ width: '100%', height: '100%' }}
//                                   resizeMode='cover'
//                                   source={item.image}
//                               />
//                           )}
                        
//                       />
//                       </View>
//                     </View>
//         </View>
//         <View style={styles.container}>
//           <Text style={styles.device_detail}>Enter Device Details</Text>
//           <Text style={styles.label}>When did you purchase your device (DD/MM/YYYY)?</Text>
//           <DatePicker
//             style={[styles.datePickerStyle],{width:'100%',marginVertical:10,borderRadius:5,backgroundColor:'#fff',}}
//             date={this.state.date} // Initial date from state
//             value={this.state.date}
//             mode="date" // The enum of date, datetime and time
//             placeholder="select date"
//             format="DD-MM-YYYY"
//             // minDate="01-01-2016"
//             // maxDate="01-01-2019"
//             confirmBtnText="Confirm"
//             cancelBtnText="Cancel"
//             customStyles={{
//               dateIcon: {
//                 //display: 'none',
//                 position: 'absolute',
//                 right: 0,
//                 top: 4,
//                 marginRight: 0,
//               },
//               dateInput: {
//                 marginRight: 36,
//                 borderRadius:5,
//                 borderColor:'#00afef'
//               },
//             }}
//             onDateChange={date => this.setState({date})}
//           />
//           <Text style={styles.label}>How much did you buy it for?</Text>
//           <TextInput style = {[styles.input]}
//                   underlineColorAndroid = "transparent"
//                   placeholder = "How much did you buy it for?"
//                   placeholderTextColor = "#aaa"
//                   autoCapitalize = "none"
//                   onChangeText = {this.mobile}
//           />
//           <Text style={styles.label}>Select your mobile brand</Text>
//           <Dropdown
//             style={{ width: '100%', alignSelf: 'center',marginVertical:10,backgroundColor:'#fff' }}
            
//             onSelect={() => {}}
//             items={['option 1', 'option 2']}
//           />
          
//           <Button
//               style={[styles.demoButton]}
//               primary
//               caption="See Best Plan"
//               onPress={() => {}}
//             />
//         </View>
//         <View style={styles.container}>
//           <Text style={styles.device_detail}>Testimonials</Text>
//         </View>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     // flex: 1,
//     // alignItems: 'center',
//     // justifyContent: 'space-around',
//     paddingHorizontal:15
//   },
//   bgImage: {
//     flex: 1,
//     marginHorizontal: -20,
//   },
//   section: {
//     flex: 1,
//     paddingHorizontal: 20,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   sectionLarge: {
//     flex: 2,
//     justifyContent: 'space-around',
//   },
//   sectionHeader: {
//     marginBottom: 8,
//   },
//   priceContainer: {
//     alignItems: 'center',
//   },
//   description: {
//     padding: 15,
//     lineHeight: 25,
//   },
//   titleDescription: {
//     color: '#19e7f7',
//     textAlign: 'center',
//     fontFamily: fonts.primaryRegular,
//     fontSize: 15,
//   },
//   title: {
//     marginTop: 30,
//   },
//   price: {
//     marginBottom: 5,
//   },
//   priceLink: {
//     borderBottomWidth: 1,
//     borderBottomColor: colors.primary,
//   },
//   image: {
//     height:257,
//     width:'100%', 
//     resizeMode:"contain"
//   },
//   device_detail: {
//     fontSize:22,
//     fontWeight:'bold',
//     textAlign:'center',
//     marginVertical:20,
//     color:'#00afef'
//   },
//   label: {
//     // paddingHorizontal:15
//   },
//   input: {
//     // margin: 15,
//     marginVertical:10,
//     height: 40,
//     color:'#fff',
//     borderColor: '#00afef', 
//     borderWidth: 1,
//     width:'100%',
//     backgroundColor:'#fff',
//     borderRadius:5
//  },
// });
