import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  FlatList,
  ScrollView,
  ActivityIndicator
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import { Button, RadioGroup, Dropdown } from '../../components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Icon } from 'react-native-elements';
import { slider_list } from '../../Api/slider.api';
import { url } from '../../configs/constants.config';
import { user_insurance } from '../../Api/insurance.api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';




export default class HomeScreen extends Component {
  
    state = {
      slider: [ ],
      insurance: [ ],
      date : new Date(),
      user_detail:'',
      user_name:'',
      loading: false
    }


    getSlider = async () => {

      let response = await slider_list();
      if(response.status){
          this.setState({slider: response.data.data});
              }  
      
      }
      getInsurance = async () => {

        let response = await user_insurance();
        if(response.status){
          this.setState({insurance: response.data.data.data});
        }  
        
        
      }

      async componentDidMount () {
        let token = await AsyncStorage.getItem('@token');

        let user = await AsyncStorage.getItem('@user');
        let type = await AsyncStorage.getItem('@type');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});

       
        console.log('token', type);
          this.getSlider();
          this.getInsurance();
      }

     
  
  

  render(){
    
    const image_url = url + 'images/slider/';
    const { width } = Dimensions.get('window');
    const scrolla = true;
    let { navigation } = this.props;
    setTimeout(() => {
      this.setState({ loading: true })
    }, 1000)
    
     if (this.state.loading) {
    
    return (
      <View>
        <StatusBar backgroundColor="#00afef" barStyle="light-content" />
        
        <TouchableOpacity onPress={() => navigation.push('Profile')}>
          <View style={{backgroundColor:"#00afef",flexDirection:'row',paddingHorizontal:15,paddingVertical:10}}>
            <View style={{borderRadius:50,overflow:'hidden',width:70,height:70,justifyContent:'center',backgroundColor:'#fff'}}>
              <Image source={this.state.user_detail.image ? {uri : url + this.state.user_detail.image} : require('../../../assets/images/RNS_nerd.png')} style={styles.profile_image} />
            </View>
            <View style={{paddingHorizontal:15,justifyContent:'center'}}>
              <Text style={{fontSize:22,color:'#fff'}}>Welcome !</Text>
              <Text style={{fontSize:22,color:'#fff',fontWeight:'bold'}}>{this.state.user_detail.fname}</Text>
            </View>
          </View>
        </TouchableOpacity>
        {/* <Image style={styles.image} source={require('../../../assets/images/avatar.png')} /> */}
        <View style={{paddingHorizontal:10}}>
          <View style={{ height: 156,marginTop: 10 }}>
            <View style={{flex: 1}}>
              <Carousel
                ref={this.carouselRef}
                layout='default'
                data={this.state.slider}
                sliderWidth={width -20}
                itemWidth={width -20}
                renderItem={({ item, index }) => ( 
                                   
                    <Image
                        key={index}
                        style={{ width: '100%', height: '100%',borderRadius:5 }}
                        resizeMode='cover'
                        source={{uri :image_url + item.image}}
                    />
                )}              
            />
            </View>
          </View>
        </View>
        <View style={styles.add_address_btn_position}>
                <TouchableOpacity onPress={() => navigation.push('FilterForm')}>
                    <View style={styles.add_address_btn}>
                        <Icon type="materialicon" color={colors.white} name="add" />
                    </View>
                </TouchableOpacity>
            </View>
        
            <View style={{marginVertical:10,paddingHorizontal:10}}>
              <Text style={{fontSize:22,fontWeight:'bold',paddingVertical:10}}>Your Insurance</Text>

              <ScrollView contentContainerStyle={styles.contentContainer}> 
                <View>

                  {
                    this.state.insurance.map((item, index) => {
                      return(
                        <TouchableOpacity onPress={() => navigation.push('InsuranceScreen', {'role':'Admin',item})}>
                          <View style={styles.order_history_content} key={index}>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:.8}}>
                                    <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>Insurance ID :</Text> #{item.invoice_pre}{item.invoice_no}</Text>                      
                                </View>
                                <View style={{flex:.4}}>
                                    <Text style={{textAlign:'right',fontSize:17}}>
                                      { moment(item.created_at).format('MMM D, YYYY') }
                                      </Text>
                                </View>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:.6}}>
                                    <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>Brand :</Text> {item.plan_brand}</Text>
                                </View>
                                
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:.6}}>
                                    <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>Payment Status :</Text> {item.payment_status}</Text>                              
                                </View>
                            </View>
                            {/* <View style={{flexDirection:'row'}}>
                                <View style={{flex:.6}}>
                                    <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>IMEI :</Text> {item.imei_number1}</Text>
                                </View>
                                <View style={{flex:.6}}>
                                    <Text style={{textAlign:'right',fontSize:17}}><Text style={{fontWeight:'bold'}}>IMEI :</Text> {item.imei_number2} </Text>
                                </View>
                            </View> */}
                            {/* <View style={{flexDirection:'row'}}>
                                <View style={{flex:.6}}>
                                    <Text style={styles.order_id}>IMEI number : {item.imei_number1}</Text>
                                </View>
                                <View style={{flex:.6}}>
                                    <Text style={styles.order_date}>IMEI number : {item.imei_number2} </Text>
                                </View>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:.6}}>
                                    <Text style={styles.order_id}>Payment : ₹{item.payment}</Text>
                                </View>
                                <View style={{flex:.6}}>
                                    <Text style={styles.order_date}>
                                    Payment Mode : {item.payment_mode}
                                      </Text>
                                </View>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:.6}}>
                                    <Text style={styles.order_id}>Payment Status : {item.payment_status}</Text>
                                </View>
                            </View> */}
                            
                          </View>
                        </TouchableOpacity>
                      );
                    })
                  }
                </View>
              </ScrollView>
            </View>
            
        </View>
    );
    } else {
      return (
        <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
          <ActivityIndicator  color={colors.primary} />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    paddingBottom: 600
  },
  container: {
    // flex: 1,
    // alignItems: 'center',
    // justifyContent: 'space-around',
    paddingHorizontal:15
  },
  bgImage: {
    flex: 1,
    marginHorizontal: -20,
  },
  section: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionLarge: {
    flex: 2,
    justifyContent: 'space-around',
  },
  sectionHeader: {
    marginBottom: 8,
  },
  priceContainer: {
    alignItems: 'center',
  },
  description: {
    padding: 15,
    lineHeight: 25,
  },
  titleDescription: {
    color: '#19e7f7',
    textAlign: 'center',
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  title: {
    marginTop: 30,
  },
  price: {
    marginBottom: 5,
  },
  priceLink: {
    borderBottomWidth: 1,
    borderBottomColor: colors.primary,
  },
  profile_image: {
    width:'100%',
    resizeMode:"center",
    height: 60,
    // borderRadius:50,
    
  },
  image: {
    height:257,
    width:'100%', 
    resizeMode:"contain"
  },
  device_detail: {
    fontSize:22,
    fontWeight:'bold',
    textAlign:'center',
    marginVertical:20,
    color:'#00afef'
  },
  label: {
    // paddingHorizontal:15
  },
  input: {
    // margin: 15,
    marginVertical:10,
    height: 40,
    color:'#fff',
    borderColor: '#00afef', 
    borderWidth: 1,
    width:'100%',
    backgroundColor:'#fff',
    borderRadius:5
 },
 
order_history_content: {
marginVertical:5,
backgroundColor: colors.white,
borderRadius:5,
padding:10,
shadowColor: "#000",
shadowOffset: {
    width: 0,
    height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,
},
order_id: {
fontSize:17,
// fontWeight:'bold'
color:'#000'
},
order_date: {
// fontSize:27,
// fontWeight:'bold',
fontSize:17,
// textAlign:'right'
},
product_name: {
fontSize:17,
fontWeight: 'bold',
// color:colors.darkgrey
},
product_price: {
// fontSize:17,
// fontWeight: 'bold'
textAlign:'right',
color:colors.darkgrey
},
add_mobile_inc_position:{
  position:'absolute',
  top:'60%',
  right:'10%',
  
  backgroundColor:'red'
},
add_address_btn_position: {
  position:'absolute',
 top:'60%',
  right:'5%',
  zIndex:10
},
add_address_btn: {
  backgroundColor: '#00afef',
  // alignItems:'center',
  borderRadius: 50,
  width: 60,
  height: 60,
  justifyContent:'center',
  
  shadowColor: "#000",
  shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.29,
  shadowRadius: 4.65,
  
  elevation: 7,
  
},
});
