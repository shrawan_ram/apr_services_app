import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  Alert,
  ActivityIndicator,
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import { Button, RadioGroup, Dropdown } from '../../components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import SelectDropdown from 'react-native-select-dropdown';
import { brand_list, plan_list } from '../../Api/insurance.api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Icon } from "react-native-elements";
import { admin_change_password,  user_change_password } from '../../Api/auth.api';

export default class ChangePasswordScreen extends Component {
  
    state = {
        user_detail         : '',
        current_pw          : '',
        new_pw              : '',
        confirm_pw          : '',
        errorCurrentpassword: true,
        errorNewpassword    : true,
        errorConfirmpassword: true,
        current_pw_secure   : true, 
        new_pw_secure       : true, 
        confirm_pw_secure   : true, 
        loading             : false
    }
  
    ChangePassword = async () => {
        let data = {
            current_password : this.state.current_pw,
            new_password : this.state.new_pw,
            confirm_password : this.state.confirm_pw
        }
        let role_id = this.state.user_detail.role_id;
        let response = '';
        if(this.state.current_pw != ''){
            this.setState({errorCurrentpassword: true});
          } else {
            this.setState({errorCurrentpassword: false});
          }
        if(this.state.new_pw != ''){
            this.setState({errorNewpassword: true});
          } else {
            this.setState({errorNewpassword: false});
          }
          if(this.state.confirm_pw != ''){
            this.setState({errorConfirmpassword: true});
          } else {
            this.setState({errorConfirmpassword: false});
          }
          if(this.state.new_pw == '' || this.state.confirm_pw == '' || this.state.current_pw == ''){
            
            
      
          } else {
                if(this.state.new_pw != this.state.confirm_pw){         
                
                    alert('Please enter the same password in both password fields');
                } else{

                if(role_id == '4'){
                    response = await user_change_password(data);
                } else {
                    response = await admin_change_password(data);
                }
                if (response.data.status == false) {
                    alert(response.data.message);
                  }

            }
        }
    //   let response = await change_password(data);
    console.log('data',response);
      if(response.data.status){
        this.props.navigation.push('Homes'); 
              }      
    }
    currentEyePress () {
        if(this.state.current_pw_secure == true){
            this.setState({current_pw_secure : false});
        } else {
            this.setState({current_pw_secure : true});
        }
    }
    newEyePress () {
        if(this.state.new_pw_secure == true){
            this.setState({new_pw_secure : false});
        } else {
            this.setState({new_pw_secure : true});
        }
    }
    confirmEyePress () {
        if(this.state.confirm_pw_secure == true){
            this.setState({confirm_pw_secure : false});
        } else {
            this.setState({confirm_pw_secure : true});
        }
    }  
     

      async componentDidMount () {
        let device_detail = await AsyncStorage.getItem('@device_detail');
        device_detail = device_detail !== null ? device_detail : '';
        this.setState({device_detail: device_detail});

        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
        
        if(this.state.date == ''){
          this.setState({date: this.state.c_year+'-'+this.state.c_month+'-'+this.state.c_date});
        }
      }

  render(){

    const { width } = Dimensions.get('window');
    let {navigation} = this.props;
    
    setTimeout(() => {
        this.setState({ loading: true })
      }, 1000)
      
    if (this.state.loading) {
    return (
      <View >
        <StatusBar backgroundColor="#00afef" barStyle="light-content" />
        
        <View style={styles.container}>
          <View style={{marginVertical:20}}>

          
                <Text style={styles.label}>Current Password</Text>
                <View >
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Current Password"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            secureTextEntry={this.state.current_pw_secure}
                            value={this.state.current_pw}  
                            onChangeText={current_pw => this.setState({ current_pw })}
                        
                    />
                    <View  style={{position:'absolute',right:20,bottom:20}}>
                        <Icon onPress={() => this.currentEyePress()} type="octicon" color='#a1a1a1' size={20}  name={this.state.current_pw_secure == true ? 'eye-closed' : 'eye'} />
                    </View>
                </View>
                {
                  !this.state.errorCurrentpassword ? <><Text  style={styles.errorMessage}>Please Enter a Current Password</Text></> : <></>
                }
                
                <Text style={styles.label}>New Password</Text>
                <View >
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "New Password"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            secureTextEntry={this.state.new_pw_secure}
                            value={this.state.new_pw}  
                            onChangeText={new_pw => this.setState({ new_pw })}
                        
                    />
                    <View  style={{position:'absolute',right:20,bottom:20}}>
                        <Icon onPress={() => this.newEyePress()} type="octicon" color='#a1a1a1' size={20}  name={this.state.new_pw_secure == true ? 'eye-closed' : 'eye'} />
                    </View>
                </View>
                {
                  !this.state.errorNewpassword ? <><Text  style={styles.errorMessage}>Please Enter a New Password</Text></> : <></>
                }
                <Text style={styles.label}>Confirm Password</Text>
                <View >
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Confirm Password"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            secureTextEntry={this.state.confirm_pw_secure}
                            value={this.state.confirm_pw}  
                            onChangeText={confirm_pw => this.setState({ confirm_pw })}
                        
                    />
                    <View  style={{position:'absolute',right:20,bottom:20}}>
                        <Icon onPress={() => this.confirmEyePress()} type="octicon" color='#a1a1a1' size={20}  name={this.state.confirm_pw_secure == true ? 'eye-closed' : 'eye'} />
                    </View>
                </View>
                {
                  !this.state.errorConfirmpassword ? <><Text  style={styles.errorMessage}>Please Enter a Confirm Password</Text></> : <></>
                }
                <Button
                    style={[styles.demoButton]}
                    
            
                    caption="Change Password"
                    onPress={() => this.ChangePassword()}
                    />
                     
                        
                </View>
        </View>
        
      </View>
    );
} else {
    return (
      <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
        <ActivityIndicator  color={colors.primary} />
      </View>
    )
  }
  }
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // alignItems: 'center',
        // justifyContent: 'space-around',
        paddingHorizontal:15
    },
    input: {
        // margin: 15,
        marginVertical:10,
        height: 40,
        color:'#000',
        borderColor: '#00afef', 
        borderWidth: 1,
        width:'100%',
        backgroundColor:'#fff',
        borderRadius:5
    },
    passwordContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#000',
        paddingBottom: 0,
      },
      inputStyle: {
        flex: 1,
      },
      errorMessage:{
        alignSelf:'flex-start',
        color:'red',
        paddingBottom:5
    }
 });


