import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ImageBackground,
    StatusBar,
    SafeAreaView,
    Pressable,
  } from 'react-native';
  
import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { Button, Dropdown } from '../../components';
import { login, sendOtp } from '../../Api/auth.api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RadioGroup from 'react-native-radio-buttons-group';
import validate from '../validation/Validation';



export default class LoginScreen extends Component {

  state = {
    mobile    : '',
    errorMobile: true,
    password  : '',
    errorPassword: true,
    radioButtonsData: [{
          id: '1', // acts as primary key, should be unique and non-empty string
          label: 'User',
          color:'#fff',
          
      }, {
          id: '2',
          label: 'Admin',
          selected: true,
          color:'#fff',
      }],
  }

  login = async () => {
    let type = '';
    this.state.radioButtonsData.forEach(element => {
      if (element.selected) {
        type = element.label;       
    }
    });
    let data = {
      mobile : this.state.mobile,
      password : this.state.password,
      type : type
  }
    
    
  // console.log('data',data);
  let response = '';
  if(this.state.mobile != ''){
    this.setState({errorMobile: true});
  } else {
    this.setState({errorMobile: false});
  }
  if(this.state.password != ''){
    this.setState({errorPassword: true});
  } else {
    this.setState({errorPassword: false});
  }
  if(this.state.mobile == '' || this.state.password == ''){
    
    
      
  }else{

    response = await login(data);
  }

    // console.log('response',response);
    // console.log('res',response.data.status);
    if (response.data.status == false) {
      alert(response.data.message);
    }
    if(response.data.status){
        
        await AsyncStorage.setItem('@user', JSON.stringify(response.data.user_details));        
        await AsyncStorage.setItem('@token', response.data.token);
        // await AsyncStorage.setItem('@type', type);
        
        this.props.navigation.push('Homes'); 
    }

  }
  sendOtp = async () =>{
    let type = '';
    this.state.radioButtonsData.forEach(element => {
      if (element.selected) {
        type = element.label;       
    }
    });
    if(this.state.mobile != ''){
      this.setState({errorMobile: true});
    } else {
      this.setState({errorMobile: false});
    }
    this.setState({errorPassword:true})
    if(this.state.mobile == ''){
      
      

    } else {

      let data = {
        mobile : this.state.mobile,
        target : 'forgot_password',
        type   : type
    }
    let response = await sendOtp(data);
    console.log('response',response);
    if(response.data.status == false){
      alert(response.data.message);
    }
    if(response.data.status){
      this.props.navigation.push('OtpVerify',data);
    }
    }
  }


  
  async componentDidMount () {
      // this.login();
  }

  onPressRadioButton(radioButtonsArray) {
      console.log('radop btns: ', radioButtonsArray);
      this.setState({radioButtonsData: radioButtonsArray});
  }

  render(){
    let {navigation} = this.props;
    let item = {
      mobile: this.state.mobile,
      target: 'forgot_password'
    }

    console.log('error',this.state.errorPassword);

    return(
        <SafeAreaView style={styles.container}>            
          <ImageBackground
            source={require('../../../assets/images/background.png')}
            style={styles.bgImage}
            resizeMode="cover"
          >
              <StatusBar barStyle="light-content" backgroundColor="transparent" translucent={true}/>
            <View style={styles.section}>
                <Image style={styles.image} source={require('../../../assets/images/apr_logo.png')} />
            </View>
            <View style={{paddingHorizontal: 40, alignItems: 'center',flex:.6}}>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    selectionColor={'#fff'}
                    placeholder = "Mobile No."                
                    placeholderTextColor = "#fff"
                    autoCapitalize = "none"
                    value={this.state.mobile}  
                    onChangeText={mobile => this.setState({ mobile })}
                    
                    
                />
                {
                  !this.state.errorMobile ? <><Text  style={styles.errorMessage}>Please Enter a Mobile No.!</Text></> : <></>
                }
                
                {/* <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Email"
                    placeholderTextColor = "#fff"
                    autoCapitalize = "none"
                    onChangeText = {this.handleEmail}
                /> */}

                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Password"
                    selectionColor={'#fff'}
                    placeholderTextColor = "#fff"
                    autoCapitalize = "none"
                    secureTextEntry={true}
                    value={this.state.password}  
                    onChangeText={password => this.setState({ password })}
                    
                />
                {
                  !this.state.errorPassword ? <><Text  style={styles.errorMessage}>Please Enter a Password</Text></> : <></>
                }
                <RadioGroup 
                    radioButtons={this.state.radioButtonsData} 
                    onPress={() => this.onPressRadioButton(this.state.radioButtonsData)}
                    layout='row'
                    containerStyle={{width:'100%',color:'#fff'}}
                    
                    
                />
                
                <TouchableOpacity style={{alignSelf:'flex-end',paddingVertical:3}} onPress={() => this.sendOtp()}>

                <Text style={{textAlign:'left',fontSize:17,color:'#fff'}}>Forgot password ?</Text>
                </TouchableOpacity>
                

            
                <Button
                    style={[styles.demoButton,]}
                    color="#f58634"
                    rounded
                    caption="Login"
                    onPress={() =>   this.login()}
                />   
                <Text style={{color:'#fff',marginTop:99}}>Not a member?
                        <Text onPress={() => navigation.push('Register')} style={{color:'#fff',fontSize:17,paddingBottom:0}}> User Sign up now</Text>
                    
                </Text>      
            </View>
            <View style={[styles.section]}>
                 
            </View>
          </ImageBackground>
        </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
      },
      bgImage: {
        flex: 1,
        marginHorizontal: -20,
        position:'absolute',
        left:0,
        top:0,
        right:0,
        bottom:0
      },
      section: {
        flex: 1,
        // marginVertical:300,
        paddingHorizontal: 40,
        justifyContent: 'center',
        alignItems: 'center',
      },
      section_a: {
        flex: .4,
        paddingHorizontal: 40,
        alignItems: 'center',
      },
      sectionLarge: {
        flex: 2,
        justifyContent: 'space-around',
      },
      sectionHeader: {
        marginBottom: 8,
      },
      priceContainer: {
        alignItems: 'center',
      },
      description: {
        padding: 15,
        lineHeight: 25,
      },
      titleDescription: {
        color: '#19e7f7',
        textAlign: 'center',
        fontFamily: fonts.primaryRegular,
        fontSize: 15,
      },
      title: {
        marginTop: 30,
      },
      price: {
        marginBottom: 5,
      },
      priceLink: {
        borderBottomWidth: 1,
        borderBottomColor: colors.primary,
      },
      input: {
        margin: 15,
        height: 40,
        color:'#fff',
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
        width:'100%'
     },
     demoButton: {
        width:'100%',
        marginTop: 8,
        marginBottom: 8,
      },
      errorMessage:{
        color:'red',
        textAlign:'left',
        alignSelf:'flex-start'
      }

})