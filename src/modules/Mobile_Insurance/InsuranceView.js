import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  Alert,
  ActivityIndicator,
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import { Button, RadioGroup, Dropdown } from '../../components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { styles } from './style';
import SelectDropdown from 'react-native-select-dropdown';
import { brand_list, plan_list } from '../../Api/insurance.api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import { url } from '../../configs/constants.config';
import ImageView from 'react-native-image-view';

export default class InsuranceScreen extends Component {
  
    state = {
      user_detail :'',
      loading     : false
    }
  
         

      async componentDidMount () {
            let user = await AsyncStorage.getItem('@user');
            user = user !== null ? JSON.parse(user) : '';
            this.setState({user_detail: user});
      }

  render(){
    const images = [
      {
          source: {
              uri: 'https://cdn.pixabay.com/photo/2017/08/17/10/47/paris-2650808_960_720.jpg',
          },
          title: 'Paris',
          width: 806,
          height: 720,
      },
  ];

    const { width } = Dimensions.get('window');
    let {navigation, route} = this.props;
    let item = route.params.item;
    console.log('route', route.params);
    setTimeout(() => {
      this.setState({ loading: true })
    }, 2000)
    
  if (this.state.loading) {
    return (
      <ScrollView >
        <StatusBar backgroundColor="#00afef" barStyle="light-content" />        
        <View style={styles.container}>        
          <View style={styles.order_history_content} key={item.id}>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.8}}>
                    <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Insurance ID :</Text> #{item.invoice_pre}{item.invoice_no}</Text>                      
                </View>
                <View style={{flex:.4}}>
                    <Text style={{textAlign:'right',fontSize:17, fontWeight:'bold', paddingVertical:8}}>
                      { moment(item.created_at).format('MMM d, YYYY') }
                      </Text>
                </View>
            </View>
            <View style={{position:'absolute',right:5,top:50}}>
              <Image source={{uri:url + item.photo}} style={{width:100,height:100,resizeMode:'contain',borderWidth:1,borderColor:'#000',borderRadius:5}} />
            </View>

            {(() => {
              if ( this.state.user_detail.role_id == '1' || this.state.user_detail.role_id == '2'){
                        return (
                          <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Added By : </Text>
                           {item.admin ? item.admin.fname : item.user.fname} 
                           {(() => {
                          if ( item.admin ? item.admin.role_id : item.user.role_id == '2'){
                              return (
                                <Text>
                                 &nbsp;(Franchise)
                                
                                </Text>
                              )
                          }
                          if ( item.admin ? item.admin.role_id : item.user.role_id == '3'){
                            return (
                              <Text>
                               &nbsp;(Shop)
                              
                              </Text>
                            )
                        }
                        if ( item.admin ? item.admin.role_id : item.user.role_id == '4'){
                          return (
                            <Text>
                             &nbsp;(Self)
                            
                            </Text>
                          )
                      }
                        if ( item.admin ? item.admin.role_id : item.user.role_id == '5'){
                          return (
                            <Text>
                             &nbsp;(Staff)
                            
                            </Text>
                          )
                      }
                    
                    return null;
                })()}
                           </Text>
                        )
                    
                    
                    return null;
                  
                }
                })()}

            
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Name :</Text> {item.name}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Father :</Text> {item.father}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Mobile :</Text> {item.mobile}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Email :</Text> {item.email}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Address :</Text> {item.address}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Aadhar No. :</Text> {item.aadharcard}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Pan No. :</Text> {item.name}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Brand :</Text> {item.plan_brand}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Brand Price :</Text> ₹{item.plan_price}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>IMEI No. :</Text> {item.imei_number1}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>IMEI No. :</Text> {item.imei_number2}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Insurance Plan :</Text> {item.plan  ? item.plan.name : 'N/A'}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Txn Id :</Text> {item.txn_id}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Payment :</Text> ₹{item.payment}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Payment mode :</Text> {item.payment_mode}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Payment status :</Text> {item.payment_status}</Text>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Aadhar Card Images :</Text></Text>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.6}}>
                  <Image source={ item.aadharcard1 ? {uri: url + item.aadharcard1} : require('../../../assets/images/default.png')} style={styles.doc_image} />                      
                </View>
                <View style={{flex:.6}}>
                  <Image source={ item.aadharcard2 ? {uri: url + item.aadharcard2} : require('../../../assets/images/default.png')} style={styles.doc_image} />
                </View>
            </View>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Mobile Images :</Text></Text>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.6}}>
                  <Image source={ item.mobile1 ? {uri: url + item.mobile1} : require('../../../assets/images/default.png')} style={styles.doc_image} />                      
                </View>
                <View style={{flex:.6}}>
                  <Image source={ item.mobile2 ? {uri: url + item.mobile2} : require('../../../assets/images/default.png')} style={styles.doc_image} />
                </View>
            </View>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Invoice Image :</Text></Text>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.6}}>
                  <Image source={ item.invoice ? {uri: url + item.invoice} : require('../../../assets/images/default.png')} style={styles.doc_image} />                      
                </View>
                <View style={{flex:.6}}></View>     
            </View>
            {/* <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Aadhar Card Images :</Text></Text>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.6}}>
                  <Image source={require('../../../assets/images/RNS_nerd.png')} style={styles.doc_image} />                      
                </View>
                <View style={{flex:.6}}>
                  <Image source={require('../../../assets/images/RNS_nerd.png')} style={styles.doc_image} />
                </View>
            </View>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Mobile Images :</Text></Text>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.6}}>
                  <Image source={require('../../../assets/images/RNS_nerd.png')} style={styles.doc_image} />                      
                </View>
                <View style={{flex:.6}}>
                  <Image source={require('../../../assets/images/RNS_nerd.png')} style={styles.doc_image} />
                </View>
            </View>
            <Text style={styles.order_id_detail}><Text style={{fontWeight:'bold'}}>Invoice Image :</Text></Text>
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.6}}>
                  <Image source={require('../../../assets/images/RNS_nerd.png')} style={styles.doc_image} />                      
                </View>
                <View style={{flex:.6}}>
                </View>
                
            </View> */}
            
          </View>
        </View>        
      </ScrollView>
    );
  } else {
    return (
      <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
        <ActivityIndicator  color={colors.primary} />
      </View>
    )
  }
  }
}


