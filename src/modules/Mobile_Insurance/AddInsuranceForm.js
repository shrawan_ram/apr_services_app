import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  Modal,
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import { Button, RadioGroup, Dropdown } from '../../components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { styles } from './style';
import { ScrollBar } from 'react-native-ui-lib';
import { SafeAreaView } from 'react-native-safe-area-context';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
// import * as ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { admin_add_insurance, admin_update_insurance, user_add_insurance, user_update_insurance } from '../../Api/insurance.api';
import FormData from 'form-data';

import RazorpayCheckout from 'react-native-razorpay';
import { get_setting } from '../../Api/setting.api';
import { url } from '../../configs/constants.config';

export default class AddInsuranceFormScreen extends Component {
  
    state = {
        Setting           : [],
        date              : new Date(),
        photo             :null,
        invoice           :null,
        aadharcard1       :null,
        aadharcard2       :null,
        mobile1           :null,
        mobile2           :null,
        user_detail       :'',
        device_detail     :'',
        name              :'',
        father_name       :'',
        mobile            :'',
        email             :'',
        aadhar_no         :'',
        address           :'',
        price             :'',
        imei_i            :'',
        imei_ii           :'',
        modalVisible      : false,
        field             :'photo',
    }

   

    addInsurance = async () => {
        let {route} = this.props;
        let data = {
                name            : this.state.name,
                father          : this.state.father_name,
                mobile          : this.state.mobile,
                email           : this.state.email,
                aadharcard      : this.state.aadhar_no,
                address         : this.state.address,
                payment         : this.state.price,
                imei_number1    : this.state.imei_i,
                imei_number2    : this.state.imei_ii,
                plan_id         : route.params.id,
                plan_price      : this.state.device_detail.price,
                plan_brand      : this.state.device_detail.brand,
                plan_date       : this.state.device_detail.date,
                payment_mode    : 'online',
                payment_status  : 'paid',
               

        }
        data = this.createFormData(this.state.photo,this.state.invoice,this.state.aadharcard1,this.state.aadharcard2,this.state.mobile1,this.state.mobile2, data);
        let role_id = this.state.user_detail.role_id;
        let response = '';
        if(role_id == '4'){
        response = await user_add_insurance(data);
        } else {
        response = await admin_add_insurance(data);
        }
        

        // console.log("response", response);
        if (response.data.status == false) {
                alert(response.data.message);
        }

        if(response.status){
                // console.log('true');
                
                var options = {
                        description: 'Credits towards consultation',
                        image: url+'/images/setting/logo/'+this.state.Setting.logo,
                        currency: 'INR',
                        key: 'rzp_test_VCgXfND6hreTDW',
                        amount: response.data.data.payment * 100,
                        name: this.state.Setting.title,
                        prefill: {
                          email: this.state.user_detail.email,
                          contact: this.state.user_detail.mobile,
                          name: this.state.Setting.title
                        },
                        theme: {color: '#00afef'}
                      }
                     
                        RazorpayCheckout.open(options).then(async (data) => {
                                // console.log('option',data);
                        // handle success
                        let role_id = this.state.user_detail.role_id;
                        // let response = '';
                        let u_data = {
                                id : response.data.data.id,
                                txn_id : data.razorpay_payment_id
                        }
                        console.log('u_data',u_data);

                        if(role_id == '4'){
                                response = await user_update_insurance(u_data);
                        } else {
                                response = await admin_update_insurance(u_data);
                        }

                        alert(`Success: ${data.razorpay_payment_id}`);

                      }).catch((error) => {
                        // handle failure
                        console.log('Error: ', error);
                        // alert(`Error: ${error.code} | ${error.description}`);
                      });

                this.props.navigation.push('Homes');          
        } 
      
    
    }
    getSetting = async () => {
        let response = await get_setting();
        if(response.status){
                this.setState({Setting: response.data.data});
        }
    }
    createFormData = (photo,invoice,aadharcard1,aadharcard2,mobile1,mobile2, body = {}) => {
        const data = new FormData();
        
        if(photo) {
            data.append('photo', {
                name: photo.fileName,
                type: photo.type,
                uri: Platform.OS === 'ios' ? photo.uri.replace('file://', '') : photo.uri,
            });
        }
        if(invoice) {
                data.append('invoice', {
                    name: invoice.fileName,
                    type: invoice.type,
                    uri: Platform.OS === 'ios' ? invoice.uri.replace('file://', '') : invoice.uri,
                });
        }
        if(aadharcard1) {
                data.append('aadharcard1', {
                    name: aadharcard1.fileName,
                    type: aadharcard1.type,
                    uri: Platform.OS === 'ios' ? aadharcard1.uri.replace('file://', '') : aadharcard1.uri,
                });
        }
        if(aadharcard2) {
                data.append('aadharcard2', {
                    name: aadharcard2.fileName,
                    type: aadharcard2.type,
                    uri: Platform.OS === 'ios' ? aadharcard2.uri.replace('file://', '') : aadharcard2.uri,
                });
        }
        if(mobile1) {
                data.append('mobile1', {
                    name: mobile1.fileName,
                    type: mobile1.type,
                    uri: Platform.OS === 'ios' ? mobile1.uri.replace('file://', '') : mobile1.uri,
                });
        }
        if(mobile2) {
                data.append('mobile2', {
                    name: mobile2.fileName,
                    type: mobile2.type,
                    uri: Platform.OS === 'ios' ? mobile2.uri.replace('file://', '') : mobile2.uri,
                });
        }
        
        Object.keys(body).forEach((key) => {
            data.append(key, body[key]);
        });
        
        return data;
    }    
   

    handleChoosePhoto (type = 'camera', field = 'photo') {
            console.log('field',this.state.field);
        let self = this
        this.setState({modalVisible: false})
        const options = {
                noData: true,
        };
        type === 'camera' ? 
                launchCamera(options, (response) => {

                        console.log('res',response);
                        if (response.assets[0].uri) {
                                self.setState({ [this.state.field]: response.assets[0] });                                
                        }
                })
        : launchImageLibrary(options, (response) => {
                if (response.assets[0].uri) {
                        self.setState({ [this.state.field]: response.assets[0] });
                }
        });
    }

  
    async componentDidMount () {
            let {route} = this.props;

        let device_detail = await AsyncStorage.getItem('@device_detail');
        device_detail = device_detail !== null ? JSON.parse(device_detail) : '';
        this.setState({device_detail: device_detail});

        let user = await AsyncStorage.getItem('@user');
       
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
        if(user.role_id == '4'){
                this.setState({name: this.state.user_detail.fname});
                this.setState({mobile: this.state.user_detail.mobile});
                this.setState({email: this.state.user_detail.email});
                
        }
        this.setState({price: route.params.price});
        this.getSetting();
    }

  render(){
    const { width } = Dimensions.get('window');
    let {navigation, route} = this.props;
    console.log('setting',this.state.Setting);

    return (
      <SafeAreaView >
        <ScrollView>
            
        

            <View style={styles.container}>
                    
              {/* <Text style={styles.device_detail}>Enter Device Details</Text> */}
              <Text style={styles.label}>Name</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter your name"
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      editable={this.state.user_detail.role_id == '4' ? false : true}
                      value={this.state.name}  
                      onChangeText={name => this.setState({ name })}
              />
              <Text style={styles.label}>Father Name</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter father name"
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      value={this.state.father_name}  
                      onChangeText={father_name => this.setState({ father_name })}
              />
              <Text style={styles.label}>Mobile Number</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter mobile no."
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      editable={this.state.user_detail.role_id == '4' ? false : true}
                      value={this.state.mobile}  
                      onChangeText={mobile => this.setState({ mobile })}
              />
              <Text style={styles.label}>Email</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter email"
                      placeholderTextColor = "#aaa"
                //       editable={this.state.user_detail.role_id == '4' ? false : true}
                      autoCapitalize = "none"
                      value={this.state.email}  
                      onChangeText={email => this.setState({ email })}
              />
              <Text style={styles.label}>Aadhar No.</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter aadhar no."
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      value={this.state.aadhar_no}  
                      onChangeText={aadhar_no => this.setState({ aadhar_no })}
              />
              <Text style={styles.label}>Address</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter address"
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      value={this.state.address}  
                      onChangeText={address => this.setState({ address })}
              />
              <Text style={styles.label}>Price</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter price"
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      editable={false}
                      value={this.state.price}  
                      onChangeText={price => this.setState({ price })}
              />
              <Text style={styles.label}>IMEI no 1</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter IMEI no."
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      value={this.state.imei_i}  
                      onChangeText={imei_i => this.setState({ imei_i })}
              />
              <Text style={styles.label}>IMEI no. 2</Text>
              <TextInput style = {[styles.input]}
                      underlineColorAndroid = "transparent"
                      placeholder = "Enter IMEI no."
                      placeholderTextColor = "#aaa"
                      autoCapitalize = "none"
                      value={this.state.imei_ii}  
                      onChangeText={imei_ii => this.setState({ imei_ii })}
              />
              <View style={{flexDirection:'row'}}>
                <Text style={styles.choose_btn_left} onPress={() =>this.setState({modalVisible: true, field: 'photo'})} >Choose Photo</Text>
                <Text style={styles.choose_btn_right} onPress={() =>this.setState({modalVisible: true, field: 'invoice'})} >Choose Invoice</Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.choose_btn_left}  onPress={() =>this.setState({modalVisible: true, field: 'aadharcard1'})} >Aadhar Front</Text>
                <Text style={styles.choose_btn_right}  onPress={() =>this.setState({modalVisible: true, field: 'aadharcard2'})} >Aadhar Back</Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.choose_btn_left}  onPress={() =>this.setState({modalVisible: true, field: 'mobile1'})} >Choose Mobile</Text>
                <Text style={styles.choose_btn_right}  onPress={() =>this.setState({modalVisible: true, field: 'mobile2'})} >Choose Mobile</Text>
              </View>
                <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        this.setState({modalVisible: false});
                        }}
                >
                        <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                                <TouchableOpacity onPress={()=>this.handleChoosePhoto('camera', this.state.field)}>
                                        <Text style={styles.modalText}>Take Photo...</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>this.handleChoosePhoto('library')}>
                                        <Text style={styles.modalText}>Choose From library...</Text>
                                </TouchableOpacity>
                        
                        <TouchableOpacity
                        onPress={() => this.setState({modalVisible: false})}
                        >
                        <Text style={styles.textStyle}>Cancel</Text>
                        </TouchableOpacity>
                        </View>
                        </View>
                </Modal>
              
                <View style={{flexDirection:'row',marginVertical:8,backgroundColor:'#fff',borderRadius:5}}>
                        <View style={{flex:.5,justifyContent:'center'}}>
                                <Text style={{textAlign:'center',fontWeight:'bold',fontSize:17}}>PAYMENT (₹{this.state.price})</Text>
                        </View>
                        <View style={{flex:.5}}>
                        <Button
                primary
                caption="Pay and Proceed"
                onPress={() => this.addInsurance()}
                />
                        </View>
                </View>
                
        </View>
        </ScrollView>
        
      </SafeAreaView>
    );
  }
}


