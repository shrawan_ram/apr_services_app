import { StyleSheet } from "react-native";
import { fonts, colors } from '../../styles';

export const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // alignItems: 'center',
        // justifyContent: 'space-around',
        paddingHorizontal:15,
        paddingVertical:15
      },
      bgImage: {
        flex: 1,
        marginHorizontal: -20,
      },
      section: {
        flex: 1,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
      },
      sectionLarge: {
        flex: 2,
        justifyContent: 'space-around',
      },
      sectionHeader: {
        marginBottom: 8,
      },
      priceContainer: {
        alignItems: 'center',
      },
      description: {
        padding: 15,
        lineHeight: 25,
      },
      titleDescription: {
        color: '#19e7f7',
        textAlign: 'center',
        fontFamily: fonts.primaryRegular,
        fontSize: 15,
      },
      title: {
        marginTop: 30,
      },
      price: {
        marginBottom: 5,
      },
      priceLink: {
        borderBottomWidth: 1,
        borderBottomColor: colors.primary,
      },
      image: {
        height:257,
        width:'100%', 
        resizeMode:"contain"
      },
      device_detail: {
        fontSize:22,
        fontWeight:'bold',
        textAlign:'center',
        marginVertical:20,
        color:'#00afef'
      },
      label: {
        // paddingHorizontal:15
      },
      input: {
        // margin: 15,
        marginVertical:10,
        height: 40,
        color:'#000',
        borderColor: '#00afef', 
        borderWidth: 1,
        width:'100%',
        backgroundColor:'#fff',
        borderRadius:5
     },
     
     itemTwoContainer: {
        backgroundColor: 'white',
        marginVertical: 5,
        borderRadius:5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
      },
      itemTwoContent: {
        padding: 20,
        position: 'relative',
        marginHorizontal: Platform.OS === 'ios' ? -15 : 0,
        // height: 130,
      },
      itemTwoTitle: {
        color: colors.black,
        fontFamily: fonts.primaryBold,
        fontSize: 20,
      },
      itemTwoSubTitleWeight:{
        fontSize:25,
        fontWeight:'bold'
      },
      itemTwoSubTitle: {
        color: colors.black,
        fontFamily: fonts.primaryRegular,
        fontSize: 15,
        marginVertical: 5,
      },
      itemTwoPrice: {
        color: colors.black,
        fontFamily: fonts.primaryBold,
        fontSize: 18,
      },
      itemTwoImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        resizeMode:'contain'
      },
      itemTwoOverlay: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: '#6271da',
        opacity: 0.5,
        borderRadius:5,
        
      },
      demoButton: {
        marginBottom:20
      },
      order_history_content: {
        marginVertical:5,
        // backgroundColor: colors.white,
        borderRadius:5,
        padding:10,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 1,
        // },
        // shadowOpacity: 0.22,
        // shadowRadius: 2.22,
        
        // elevation: 3,
        },
        order_id: {
        fontSize:17,
        // fontWeight:'bold'
        paddingVertical:8,
        color:'#000'
        },
        doc_image: {
          width:'99%',
          resizeMode:'contain',
          height:158
        },

        centeredView: {
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginTop: 0,
          backgroundColor:'rgba(52, 52, 52, .3)',
          
        },
        modalView: {
          margin: 0,
          width:300,
          backgroundColor: "white",
          borderRadius: 5,
          padding: 35,
          // alignItems: "left",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2
          },
          shadowOpacity: 0.25,
          shadowRadius: 4,
          elevation: 5
        },
        button: {
          borderRadius: 20,
          padding: 10,
          elevation: 2
        },
        buttonOpen: {
          backgroundColor: "#F194FF",
        },
        buttonClose: {
          backgroundColor: "#2196F3",
        },
        textStyle: {
          color: "#000",
          fontWeight: "bold",
          textAlign: "right",
          fontSize:17,
          textTransform:'uppercase'
        },
        modalText: {
          marginBottom: 15,
          paddingVertical:8,
          fontSize:17
        },
        choose_btn_left :{
          flex:.6,
          marginRight:2,
          marginBottom:2,
          backgroundColor:'#aaa',
          textAlign:'center',
          borderRadius:5,
          paddingVertical:8
        },
        choose_btn_right :{
          flex:.6,
          marginBottom:2,
          backgroundColor:'#aaa',
          textAlign:'center',
          borderRadius:5,
          paddingVertical:8
        },
        order_history_content: {
          marginVertical:5,
          backgroundColor: colors.white,
          borderRadius:5,
          padding:10,
          shadowColor: "#000",
          shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,
          
          elevation: 3,
          },
          order_id: {
          fontSize:17,
          // fontWeight:'bold'
          color:'#000'
          },
          order_date: {
          // fontSize:27,
          // fontWeight:'bold',
          fontSize:17,
          // textAlign:'right'
          },
          order_id_detail: {
            fontSize:17,
            // fontWeight:'bold'
            paddingVertical:5,
            color:'#000'
            },
            add_address_btn_position: {
              position:'absolute',
              bottom:'10%',
              right:'5%',
              zIndex:10
            },
            add_address_btn: {
              backgroundColor: '#00afef',
              // alignItems:'center',
              borderRadius: 50,
              width: 60,
              height: 60,
              justifyContent:'center',
              alignItems:'center',
              shadowColor: "#000",
              shadowOffset: {
                  width: 0,
                  height: 3,
              },
              shadowOpacity: 0.29,
              shadowRadius: 4.65,
              
              elevation: 7,
              
            },
})