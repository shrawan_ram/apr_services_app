import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  Alert,
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import { Button, RadioGroup, Dropdown } from '../../components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { styles } from './style';
import SelectDropdown from 'react-native-select-dropdown';
import { brand_list, plan_list } from '../../Api/insurance.api';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class FilterFormScreen extends Component {
  
    state = {
      c_date : new Date().getDate(),
      c_month : new Date().getMonth() + 1,
      c_year : new Date().getFullYear(),
      date : '',
      price: '',
      brands:[],
      brand:'',
      plans : [],
      device_detail:'',
    }
  
    getBrands = async () => {

      let response = await brand_list();
      if(response.status){
          this.setState({brands: response.data.data});
              }      
    }
      
    filterPlans = async () => {
      
        let data = {
          price : this.state.price
        }
        let device_detail = {
          price : this.state.price,
          brand : this.state.brand,
          date  : this.state.date
        }
        let response = await plan_list(data);
        if (response.data.status == false) {
          alert(response.data.message);
        }
      if(response.status){
          await AsyncStorage.setItem('@device_detail', JSON.stringify(device_detail));
          
          this.setState({plans: response.data.data});
          
        } 
      
    
    }     

      async componentDidMount () {
        let device_detail = await AsyncStorage.getItem('@device_detail');
        device_detail = device_detail !== null ? device_detail : '';
        this.setState({device_detail: device_detail});
        
        this.getBrands();
        if(this.state.date == ''){
          this.setState({date: this.state.c_year+'-'+this.state.c_month+'-'+this.state.c_date});
        }
      }

  render(){

    const { width } = Dimensions.get('window');
    let {navigation} = this.props;
    

    return (
      <View >
        <StatusBar backgroundColor="#00afef" barStyle="light-content" />
        
        <View style={styles.container}>
        
          <Text style={styles.device_detail}>Enter Device Details</Text>
          <Text style={styles.label}>When did you purchase your device (DD/MM/YYYY)?</Text>
          <DatePicker
            style={[styles.datePickerStyle],{width:'100%',marginVertical:10,borderRadius:5,backgroundColor:'#fff',}}
            date={this.state.date} // Initial date from state
            value={this.state.date}
            mode="date" // The enum of date, datetime and time
            placeholder="select date"
            format="YYYY-MM-DD"
            // minDate="01-01-2016"
            // maxDate="01-01-2019"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                //display: 'none',
                position: 'absolute',
                right: 0,
                top: 4,
                marginRight: 0,
              },
              dateInput: {
                marginRight: 36,
                borderRadius:5,
                borderColor:'#00afef'
              },
            }}
            onDateChange={date => this.setState({date})}
          />
          <Text style={styles.label}>How much did you buy it for?</Text>
          <TextInput style = {[styles.input]}
                  underlineColorAndroid = "transparent"
                  placeholder = "How much did you buy it for?"
                  placeholderTextColor = "#aaa"
                  autoCapitalize = "none"
                  value={this.state.price}  
                  onChangeText={price => this.setState({ price })}
          />
          
          <Text style={styles.label}>Select your mobile brand</Text>
          
          <SelectDropdown
            data={this.state.brands}
            onSelect={brand => this.setState({ brand })}
            buttonTextStyle={{color:colors.primary,fontSize:12,textAlign:'left'}}
            buttonStyle={styles.input}
            dropdownStyle={{fontSize:12,paddingVertical:5}}
            rowTextStyle={{fontSize:12,textAlign:'left',marginVertical:5}} 

            buttonTextAfterSelection={(selectedItem, index) => {
              // text represented after item is selected
              // if data array is an array of objects then return selectedItem.property to render after item is selected
              return selectedItem
            }}
            rowTextForSelection={(item, index) => {
              // text represented for each item in dropdown
              // if data array is an array of objects then return item.property to represent item in dropdown
              return item
            }}
          />

          
          <Button
              style={[styles.demoButton]}
              primary
              caption="See Best Plan"
              onPress={() => this.filterPlans()}
            />
            {
              this.state.plans && this.state.plans.length ?
              <View style={{marginVertical:10}}>
                  {
                      this.state.plans.map((item, index) => {
                          return(
                              <TouchableOpacity
                                  key={item.id}
                                  style={styles.itemTwoContainer}
                                  onPress = {() => navigation.push('AddInsuranceForm',item)}
                                  >
                                  <View style={styles.itemTwoContent}>
                                      {/* <Image style={styles.itemTwoImage} source={item.image} /> */}
                                      <View style={styles.itemTwoOverlay} />
                                      <Text style={styles.itemTwoTitle}>{item.name}</Text>
                                      <Text style={styles.itemTwoSubTitle}><Text style={styles.itemTwoSubTitleWeight}>₹{item.price}</Text> only/year
                                          
                                      </Text>
                                      
                                      <Text style={styles.itemTwoPrice}>inclusive of all taxes
                                      {/* </Text>
                                      <Text style={styles.itemTwoPrice}> */}
                                          (an affordable ₹{item.per_month_price}/month)</Text>
                                  </View>
                              </TouchableOpacity>

                          );
                      })
                  }
              </View>
              :
              
              <View style={{marginVertical:10}}>
                {/* <Text>Record Not Found</Text> */}
              </View>

            }
        </View>
        
      </View>
    );
  }
}


