import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import { Button, RadioGroup, Dropdown } from '../../components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { styles } from './style';
import { ScrollBar } from 'react-native-ui-lib';
import { SafeAreaView } from 'react-native-safe-area-context';

export default class PlanListScreen extends Component {
  
    state = {
      Plans: [
        {
        image: require('../../../assets/images/1628316946New_Project.jpg')
      },
      {
        image: require('../../../assets/images/1628316946New_Project.jpg')
      },
      {
        image: require('../../../assets/images/1628316946New_Project.jpg')
      },
      {
        image: require('../../../assets/images/1628316946New_Project.jpg')
      },
      {
        image: require('../../../assets/images/1628316946New_Project.jpg')
      },
      {
        image: require('../../../assets/images/1628316946New_Project.jpg')
      },

      
      ],
      date : new Date(),
    }
  
  // const rnsUrl = 'https://reactnativestarter.com';
  // const handleClick = () => {
  //   Linking.canOpenURL(rnsUrl).then(supported => {
  //     if (supported) {
  //       Linking.openURL(rnsUrl);
  //     } else {
  //       console.log(`Don't know how to open URI: ${rnsUrl}`);
  //     }
  //   });
  // };

  render(){
    const { width } = Dimensions.get('window');
    let { navigation } = this.props;
    return (
      <SafeAreaView >
        <ScrollView>
            
        <StatusBar backgroundColor="#00afef" barStyle="light-content" />

            <View style={styles.container}>
                {
                    this.state.Plans.map((item, index) => {
                        return(
                            <TouchableOpacity
                                key={item.id}
                                style={styles.itemTwoContainer}
                                onPress = {() => navigation.push('AddInsuranceForm')}
                                >
                                <View style={styles.itemTwoContent}>
                                    {/* <Image style={styles.itemTwoImage} source={item.image} /> */}
                                    <View style={styles.itemTwoOverlay} />
                                    <Text style={styles.itemTwoTitle}>Plan Name</Text>
                                    <Text style={styles.itemTwoSubTitle}><Text style={styles.itemTwoSubTitleWeight}>₹999</Text> only/year
                                        
                                    </Text>
                                    
                                    <Text style={styles.itemTwoPrice}>inclusive of all taxes
                                    {/* </Text>
                                    <Text style={styles.itemTwoPrice}> */}
                                        (an affordable ₹83/month)</Text>
                                </View>
                            </TouchableOpacity>

                        );
                    })
                }
            </View>
        </ScrollView>
        
      </SafeAreaView>
    );
  }
}


