import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React, { Component } from "react";
import { ActivityIndicator, Button, FlatList, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-ionicons";
import { DataTable } from "react-native-paper";
import RBSheet from "react-native-raw-bottom-sheet";
import { admin_insurance, user_insurance } from "../../Api/insurance.api";
import { colors } from "../../styles";
import { styles } from './style';



export default class InsurancesScreen extends Component{
    
        state = {
          user_detail   :'',
          insurance     :[],
          loading       : false
        }
        getInsurance = async () => {
            let role_id = this.state.user_detail.role_id;
            let response = '';
            if(role_id == '4'){
                response = await user_insurance();
            } else {
                response = await admin_insurance();
            }
            
            
            console.log('response',response);
            if(response.status){
              this.setState({insurance: response.data.data.data});
            }  
            
            
          }
        
        async componentDidMount() {
            
            let user = await AsyncStorage.getItem('@user');
            user = user !== null ? JSON.parse(user) : '';
            this.setState({user_detail: user});
            
            // let {route} = this.props;
            // let Role = route.params.role;
            
            this.getInsurance();

            
        }

       
      
    render(){
        let {navigation,route} = this.props;
        console.log('user',this.state.insurance);
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
          
        if (this.state.loading) {
        return(
            <SafeAreaView style={{position:'relative',flex:1}}>
                
                <ScrollView>
                    <View style={{padding:10}}>                        
                        <View>
                        
                            {
                            this.state.insurance.map((item, index) => {
                                return(
                                <TouchableOpacity
                                //  onPress={() => this[RBSheet + index].open()}
                                 onPress={() => navigation.push('InsuranceScreen',{'role':'Admin',item})}
                                 >
                                    <View style={styles.order_history_content} key={index}>
                                        <View style={{flexDirection:'row'}}>
                                            <View style={{flex:.8}}>
                                                <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>Insurance ID :</Text> #{item.invoice_pre}{item.invoice_no}</Text>                      
                                            </View>
                                            <View style={{flex:.4}}>
                                                <Text style={{textAlign:'right',fontSize:17}}>
                                                { moment(item.created_at).format('MMM D, YYYY') }
                                                </Text>
                                            </View>
                                        </View>
                                        <View style={{flexDirection:'row'}}>
                                            <View style={{flex:.6}}>
                                                <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>Name :</Text> {item.name}</Text>
                                            </View>
                                            
                                        </View>
                                        <View style={{flexDirection:'row'}}>
                                            <View style={{flex:.6}}>
                                                <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>Brand :</Text> {item.plan_brand}</Text>
                                            </View>
                                            
                                        </View>
                                        <View style={{flexDirection:'row'}}>
                                            <View style={{flex:.6}}>
                                                <Text style={styles.order_id}><Text style={{fontWeight:'bold'}}>Payment Status :</Text> {item.payment_status}</Text>                              
                                            </View>
                                        </View>
                                    <RBSheet
                                        ref={ref => {
                                            this[RBSheet + index] = ref;
                                        }}
                                        draggableIcon={true}
                                        closeOnDragDown={true}
                                        height={200}
                                        customStyles={{
                                            container: {
                                            //   justifyContent: "center",
                                            //   alignItems: "center",
                                              borderTopLeftRadius:20,
                                              borderTopRightRadius:20,
                                            }
                                          }}
                                    >
                                        <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                            <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                        </View>
                                        <TouchableOpacity onPress={() => navigation.push('AddStaff',{'role':route.params.role,item})}>
                                            <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,padding:15}}>
                                                <Text style={{fontSize:18}}>Edit</Text>                                            
                                            </View>
                                        </TouchableOpacity>
                                        <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,padding:15}}>
                                            <Text style={{fontSize:18}}>Delete</Text>                                            
                                        </View>
                                        <TouchableOpacity onPress={() => navigation.push('StaffDetail',item)}>
                                            <View style={{padding:15}}>
                                                <Text style={{fontSize:18}}>View Detail</Text>                                            
                                            </View>
                                        </TouchableOpacity>
                                    </RBSheet>
                                    </View>
                                 </TouchableOpacity>
                                );
                            })
                            }
                            </View>
                            {/* <Button title="OPEN BOTTOM SHEET" onPress={() => this.RBSheet.open()} /> */}
        {/* <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={300}
          openDuration={250}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center"
            }
          }}
        >
          <YourOwnComponent />
        </RBSheet> */}

        
                    </View>                    
                </ScrollView>
                {(() => {
                    if ( this.state.user_detail.role_id != '1'){
                        return (
                            <View style={styles.add_address_btn_position}>
                                <TouchableOpacity onPress={() => navigation.push('FilterForm')}>
                                    <View style={styles.add_address_btn}>
                                        <Icon type="ionicons" color={colors.white} name="add" />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    }
                    
                    return null;
                })()}

                
                
            </SafeAreaView>
        );
    } else {
        return (
          <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
            <ActivityIndicator  color={colors.primary} />
          </View>
        )
      }
    }
}