import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  FlatList,
  ScrollView,
  ActivityIndicator,
  Modal
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import { Button, RadioGroup, Dropdown } from '../../components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Icon } from 'react-native-elements';
import { slider_list } from '../../Api/slider.api';
import { url } from '../../configs/constants.config';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
// import * as ImagePicker from 'react-native-image-picker';
import { insurance } from '../../Api/insurance.api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import { admin_editProfile,  admin_Profile,  Profile, user_editProfile, user_Profile } from '../../Api/auth.api';


export default class EditProfileScreen extends Component {
  
    state = {
      user_detail : '',
      user: [ ],
      name:'',
      email:'',
      aadhar_no:'',
      address:'',
      city:'',
      state:'',
      pincode:'',
      image:null,
      loading: false,
      modalVisible:false
    }


    Profile = async () => {

      let role_id = this.state.user_detail.role_id;
        // console.log('role',role_id);
        let response = '';
        if(role_id == '4'){
            response = await user_Profile();
        } else {
            response = await admin_Profile();
        }
        console.log('res',response);
      if(response.status){
          this.setState({user: response.data.data});
         
              } 
        let user_d = this.state.user;
              this.setState({name: user_d.fname}); 
              this.setState({email: user_d.email}); 
              this.setState({aadhar_no: user_d.aadhar_no}); 
              this.setState({address: user_d.address}); 
              this.setState({city: user_d.city}); 
              this.setState({state: user_d.state}); 
              this.setState({image: user_d.image}); 
              this.setState({pincode: user_d.pincode}); 
      
      }
    editProfile = async () => {
      let role_id = this.state.user_detail.role_id;
        

        
        let response = '';
        if(role_id == '4'){
          let data = {
            fname       : this.state.name,
            email       : this.state.email,
            aadhar_no   : this.state.aadhar_no,
            address     : this.state.address,
            city        : this.state.city,
            state       : this.state.state,
            pincode     : this.state.pincode, 
        }
        data = this.createFormData(this.state.image, data);
            response = await user_editProfile(data);
        } else {
          let data = {
            fname       : this.state.name,
            email       : this.state.email,
            aadharcard   : this.state.aadhar_no,
            address     : this.state.address,
            city        : this.state.city,
            state       : this.state.state,
            pincode     : this.state.pincode, 
        }
        data = this.createFormData(this.state.image, data);
            response = await admin_editProfile(data);
        }

        // console.log('profile',response);
        if (response.data.status == false) {
          alert(response.data.message);
        }
        if(response.data.status){
            this.props.navigation.push('Profile'); 
        } 
    }
      createFormData = (image, body = {}) => {
        const data = new FormData();
        
        if(image) {
            data.append('image', {
                name: image.fileName,
                type: image.type,
                uri: Platform.OS === 'ios' ? image.uri.replace('file://', '') : image.uri,
            });
        }
        
        
        Object.keys(body).forEach((key) => {
            data.append(key, body[key]);
        });
        
        return data;
    }    
   

    handleChoosePhoto (type = 'camera', field = 'image') {
            // console.log('field',this.state.field);
        let self = this
        this.setState({modalVisible: false})
        const options = {
                noData: true,
        };
        type === 'camera' ? 
                launchCamera(options, (response) => {

                        // console.log('res',response);
                        if (response.assets[0].uri) {
                                self.setState({ [this.state.field]: response.assets[0] });                                
                        }
                })
        : launchImageLibrary(options, (response) => {
                if (response.assets[0].uri) {
                        self.setState({ [this.state.field]: response.assets[0] });
                }
        });
    }
      

      async componentDidMount () {
        let token = await AsyncStorage.getItem('@token');
        
        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});

       
        // console.log('token', user.user_details);
          this.Profile();
      }
  
  

  render(){
    let user = this.state.user;
    const image_url = url + 'images/slider/';
    const { width } = Dimensions.get('window');
    const scrolla = true;
    let { navigation } = this.props;
    setTimeout(() => {
      this.setState({ loading: true })
    }, 1000)
    
     if (this.state.loading) {
    
    return (
      <View>
        <StatusBar backgroundColor="#00afef" barStyle="light-content" />
        <View>
          <View style={{backgroundColor:"#00afef",paddingHorizontal:15,paddingVertical:10,alignItems:'center'}}>
            <View style={{borderRadius:50,overflow:'hidden',width:100,height:100,justifyContent:'center',backgroundColor:'#fff',alignSelf:'center'}}>
              <Image source={this.state.image ? {uri: url + this.state.image} : require('../../../assets/images/RNS_nerd.png')} style={styles.profile_image} />
            </View>
              <TouchableOpacity onPress={() =>this.setState({modalVisible: true, field: 'image'})}>
                  <Text style={{fontSize:20,paddingTop:8}}>Change Profile Photo</Text>
              </TouchableOpacity>
            
          </View>
        </View>
        <View style={styles.add_address_btn_position}>
            <TouchableOpacity onPress={() => this.editProfile()}>
                <View style={styles.add_address_btn}>
                    <Icon type="materialicon" color={colors.white} name="check" />
                </View>
            </TouchableOpacity>
        </View>
        
            <View style={{marginVertical:10,paddingHorizontal:10}}>
              <Text style={{fontSize:22,fontWeight:'bold',paddingVertical:10}}>Details :</Text>

              <ScrollView contentContainerStyle={styles.contentContainer}> 
                <View>               
                    <Text style={styles.label}>Name</Text>
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Enter your name"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            // editable={false}
                            value={this.state.name}  
                            onChangeText={name => this.setState({ name })}
                    />
                    <Text style={styles.label}>Email</Text>
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Enter your email"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            // editable={false}
                            value={this.state.email}  
                            onChangeText={email => this.setState({ email })}
                    />
                    <Text style={styles.label}>Aadhar No.</Text>
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Enter your aadhar no"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            // editable={false}
                            value={this.state.aadhar_no}  
                            onChangeText={aadhar_no => this.setState({ aadhar_no })}
                    />
                    <Text style={styles.label}>Address</Text>
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Enter your address"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            // editable={false}
                            value={this.state.address}  
                            onChangeText={address => this.setState({ address })}
                    />
                    <Text style={styles.label}>City</Text>
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Enter your email"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            // editable={false}
                            value={this.state.city}  
                            onChangeText={city => this.setState({ city })}
                    />
                    <Text style={styles.label}>State</Text>
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Enter your State"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            // editable={false}
                            value={this.state.state}  
                            onChangeText={state => this.setState({ state })}
                    />
                    <Text style={styles.label}>Pincode</Text>
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Enter your pincode"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            // editable={false}
                            value={this.state.pincode}  
                            onChangeText={pincode => this.setState({ pincode })}
                    />
                </View>
              </ScrollView>
            </View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                this.setState({modalVisible: false});
                }}
            >
                    <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                            <TouchableOpacity onPress={()=>this.handleChoosePhoto('camera', this.state.field)}>
                                    <Text style={styles.modalText}>Take Photo...</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>this.handleChoosePhoto('library')}>
                                    <Text style={styles.modalText}>Choose From library...</Text>
                            </TouchableOpacity>
                    
                    <TouchableOpacity
                    onPress={() => this.setState({modalVisible: false})}
                    >
                    <Text style={styles.textStyle}>Cancel</Text>
                    </TouchableOpacity>
                    </View>
                    </View>
            </Modal>
            
        </View>
    );
    } else {
      return (
        <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
          <ActivityIndicator  color={colors.primary} />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    paddingBottom: 400
  },
  container: {
    // flex: 1,
    // alignItems: 'center',
    // justifyContent: 'space-around',
    paddingHorizontal:15
  },
  bgImage: {
    flex: 1,
    marginHorizontal: -20,
  },
  section: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionLarge: {
    flex: 2,
    justifyContent: 'space-around',
  },
  sectionHeader: {
    marginBottom: 8,
  },
  priceContainer: {
    alignItems: 'center',
  },
  description: {
    padding: 15,
    lineHeight: 25,
  },
  titleDescription: {
    color: '#19e7f7',
    textAlign: 'center',
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  title: {
    marginTop: 30,
  },
  price: {
    marginBottom: 5,
  },
  priceLink: {
    borderBottomWidth: 1,
    borderBottomColor: colors.primary,
  },
  profile_image: {
    width:'100%',
    resizeMode:"center",
    height: 60,
    // borderRadius:50,
    
  },
  image: {
    height:257,
    width:'100%', 
    resizeMode:"contain"
  },
  device_detail: {
    fontSize:22,
    fontWeight:'bold',
    textAlign:'center',
    marginVertical:20,
    color:'#00afef'
  },
  label: {
    // paddingHorizontal:15
  },
  input: {
    // margin: 15,
    marginVertical:10,
    height: 40,
    color:'#000',
    borderColor: '#00afef', 
    borderWidth: 1,
    width:'100%',
    backgroundColor:'#fff',
    borderRadius:5
 },
 
order_history_content: {
marginVertical:5,
backgroundColor: colors.white,
borderRadius:5,
padding:10,
shadowColor: "#000",
shadowOffset: {
    width: 0,
    height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,
},
order_id: {
fontSize:17,
// fontWeight:'bold'
color:'#000'
},
order_date: {
// fontSize:27,
// fontWeight:'bold',
fontSize:17,
// textAlign:'right'
},
product_name: {
fontSize:17,
fontWeight: 'bold',
// color:colors.darkgrey
},
product_price: {
// fontSize:17,
// fontWeight: 'bold'
textAlign:'right',
color:colors.darkgrey
},

add_address_btn_position: {
    position:'absolute',
   bottom:'25%',
    right:'5%',
    zIndex:10
  },
  add_address_btn: {
    backgroundColor: '#00afef',
    // alignItems:'center',
    borderRadius: 50,
    width: 60,
    height: 60,
    justifyContent:'center',
    
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    
    elevation: 7,
    
  },
centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 0,
    backgroundColor:'rgba(52, 52, 52, .3)',
    
  },
  modalView: {
    margin: 0,
    width:300,
    backgroundColor: "white",
    borderRadius: 5,
    padding: 35,
    // alignItems: "left",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "#000",
    fontWeight: "bold",
    textAlign: "right",
    fontSize:17,
    textTransform:'uppercase'
  },
  modalText: {
    marginBottom: 15,
    paddingVertical:8,
    fontSize:17
  },
  profile_image: {
    width:'100%',
    resizeMode:"cover",
    height: 100,
    // borderRadius:50,
    
  },
});
