import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  StatusBar,
  FlatList,
  ScrollView,
  ActivityIndicator
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';
import { TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import { Button, RadioGroup, Dropdown } from '../../components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Icon } from 'react-native-elements';
import { slider_list } from '../../Api/slider.api';
import { url } from '../../configs/constants.config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import { admin_Profile, Profile, user_Profile } from '../../Api/auth.api';


export default class ProfileScreen extends Component {
  
    state = {
      user: [ ],
      date : new Date(),
      user_detail:'',
      user_name:'',
      loading: false
    }


    Profile = async () => {

        let role_id = this.state.user_detail.role_id;
        // console.log('role',role_id);
        let response = '';
        if(role_id == '4'){
            response = await user_Profile();
        } else {
            response = await admin_Profile();
        }
      if(response.status){
          this.setState({user: response.data.data});
              }  
      
      }
      

      async componentDidMount () {
        let token = await AsyncStorage.getItem('@token');

        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});

       
        // console.log('token', user.user_details);
          this.Profile();
      }
  
  

  render(){
    // console.log('user',this.state.user);
    let user = this.state.user;
    const image_url = url + 'images/slider/';
    const { width } = Dimensions.get('window');
    const scrolla = true;
    let { navigation } = this.props;
    setTimeout(() => {
      this.setState({ loading: true })
    }, 1000)
    
     if (this.state.loading) {
    
    return (
      <View>
        <StatusBar backgroundColor="#00afef" barStyle="light-content" />
        <View>
          <View style={{backgroundColor:"#00afef",flexDirection:'row',paddingHorizontal:15,paddingVertical:10}}>
            <View style={{borderRadius:50,overflow:'hidden',width:70,height:70,justifyContent:'center',backgroundColor:'#fff'}}>
              <Image source={ user.image ? {uri: url + user.image} : require('../../../assets/images/RNS_nerd.png')} style={styles.profile_image} />
            </View>
            <View style={{paddingHorizontal:15,justifyContent:'center'}}>
              <Text style={{fontSize:22,color:'#fff'}}>Welcome !</Text>
              <Text style={{fontSize:22,color:'#fff',fontWeight:'bold'}}>{this.state.user_detail.fname}</Text>
            </View>
          </View>
        </View>
        {/* <Image style={styles.image} source={require('../../../assets/images/avatar.png')} /> */}
        <View style={styles.add_address_btn_position}>
            <TouchableOpacity onPress={() => navigation.push('EditProfile')}>
                <View style={styles.add_address_btn}>
                    <Icon type="materialicon" color={colors.white} name="edit" />
                </View>
            </TouchableOpacity>
        </View>
        
            <View style={{marginVertical:10,paddingHorizontal:10}}>
              <Text style={{fontSize:22,fontWeight:'bold',paddingVertical:10}}>Details :</Text>

              <ScrollView contentContainerStyle={styles.contentContainer}> 
                <View>
                {
                    user.email ?
                        
                    <View style={{flexDirection:'row',marginBottom:25}}>
                        <View style={{flex:.2}}>
                            <Icon type="materialicon" style={{borderWidth:1,borderColor:'#aaa',borderRadius:50,height:40,width:40,justifyContent:'center'}} color={colors.primary} name="mail" />                  
                        </View>
                        <View style={{flex:.8}}>
                        <Text >Email</Text>
                            <Text style={{fontWeight:'bold',fontSize:17}}>
                            {user.email}
                                </Text>
                        </View>
                    </View>
                      :
                      <></>  
                    
                }
                {
                    user.mobile ?
                        <View style={{flexDirection:'row',marginBottom:25}}>
                            <View style={{flex:.2}}>
                                <Icon type="materialicon" style={{borderWidth:1,borderColor:'#aaa',borderRadius:50,height:40,width:40,justifyContent:'center'}} color={colors.primary} name="phone" />                  
                            </View>
                            <View style={{flex:.8}}>
                            <Text style={styles.order_id}><Text >Mobile No</Text></Text>
                                <Text style={{fontWeight:'bold',fontSize:17}}>
                                {user.mobile}
                                    </Text>
                            </View>
                        </View>
                        :
                        <></>
                }
                {
                    user.aadhar_no ?
                        <View style={{flexDirection:'row',marginBottom:25}}>
                            <View style={{flex:.2}}>
                                <Icon type="materialicon" style={{borderWidth:1,borderColor:'#aaa',borderRadius:50,height:40,width:40,justifyContent:'center'}} color={colors.primary} name="credit-card" />                  
                            </View>
                            <View style={{flex:.8}}>
                            <Text style={styles.order_id}><Text >Aadhar No.</Text></Text>
                                <Text style={{fontWeight:'bold',fontSize:17}}>
                                {user.aadhar_no}
                                    </Text>
                            </View>
                        </View>
                        :
                        <></>
                }
                {
                    user.address ?
                        <View style={{flexDirection:'row',marginBottom:25}}>
                            <View style={{flex:.2}}>
                                <Icon type="materialicon" style={{borderWidth:1,borderColor:'#aaa',borderRadius:50,height:40,width:40,justifyContent:'center'}} color={colors.primary} name="location-on" />                  
                            </View>
                            <View style={{flex:.8}}>
                            <Text style={styles.order_id}><Text >Address</Text></Text>
                                <Text style={{fontWeight:'bold',fontSize:17}}>
                                {user.address}
                                    </Text>
                            </View>
                        </View>
                        :
                        <></>
                }
                {
                    user.city ?
                        <View style={{flexDirection:'row',marginBottom:25}}>
                            <View style={{flex:.2}}>
                                <Icon type="materialicon" style={{borderWidth:1,borderColor:'#aaa',borderRadius:50,height:40,width:40,justifyContent:'center'}} color={colors.primary} name="location-city" />                  
                            </View>
                            <View style={{flex:.8}}>
                            <Text style={styles.order_id}><Text >City</Text></Text>
                                <Text style={{fontWeight:'bold',fontSize:17}}>
                                {user.city}
                                    </Text>
                            </View>
                        </View>
                        :
                        <></>
                }
                {
                    user.state ?
                        <View style={{flexDirection:'row',marginBottom:25}}>
                            <View style={{flex:.2}}>
                                <Icon type="materialicon" style={{borderWidth:1,borderColor:'#aaa',borderRadius:50,height:40,width:40,justifyContent:'center'}} color={colors.primary} name="location-city" />                  
                            </View>
                            <View style={{flex:.8}}>
                            <Text style={styles.order_id}><Text >State</Text></Text>
                                <Text style={{fontWeight:'bold',fontSize:17}}>
                                {user.state}
                                    </Text>
                            </View>
                        </View>
                        :
                        <></>
                }
                {
                    user.pincode ?
                        <View style={{flexDirection:'row',marginBottom:25}}>
                            <View style={{flex:.2}}>
                                <Icon type="materialicon" style={{borderWidth:1,borderColor:'#aaa',borderRadius:50,height:40,width:40,justifyContent:'center'}} color={colors.primary} name="location-city" />                  
                            </View>
                            <View style={{flex:.8}}>
                            <Text style={styles.order_id}><Text >Pincode</Text></Text>
                                <Text style={{fontWeight:'bold',fontSize:17}}>
                                {user.pincode}
                                    </Text>
                            </View>
                        </View>
                        :
                        <></>
                }

                  
                </View>
              </ScrollView>
            </View>
            
        </View>
    );
    } else {
      return (
        <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
          <ActivityIndicator  color={colors.primary} />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    paddingBottom: 600
  },
  container: {
    // flex: 1,
    // alignItems: 'center',
    // justifyContent: 'space-around',
    paddingHorizontal:15
  },
  bgImage: {
    flex: 1,
    marginHorizontal: -20,
  },
  section: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionLarge: {
    flex: 2,
    justifyContent: 'space-around',
  },
  sectionHeader: {
    marginBottom: 8,
  },
  priceContainer: {
    alignItems: 'center',
  },
  description: {
    padding: 15,
    lineHeight: 25,
  },
  titleDescription: {
    color: '#19e7f7',
    textAlign: 'center',
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  title: {
    marginTop: 30,
  },
  price: {
    marginBottom: 5,
  },
  priceLink: {
    borderBottomWidth: 1,
    borderBottomColor: colors.primary,
  },
  profile_image: {
    width:'100%',
    resizeMode:"center",
    height: 60,
    // borderRadius:50,
    
  },
  image: {
    height:257,
    width:'100%', 
    resizeMode:"contain"
  },
  device_detail: {
    fontSize:22,
    fontWeight:'bold',
    textAlign:'center',
    marginVertical:20,
    color:'#00afef'
  },
  label: {
    // paddingHorizontal:15
  },
  input: {
    // margin: 15,
    marginVertical:10,
    height: 40,
    color:'#fff',
    borderColor: '#00afef', 
    borderWidth: 1,
    width:'100%',
    backgroundColor:'#fff',
    borderRadius:5
 },
 
order_history_content: {
marginVertical:5,
backgroundColor: colors.white,
borderRadius:5,
padding:10,
shadowColor: "#000",
shadowOffset: {
    width: 0,
    height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,
},
order_id: {
fontSize:17,
// fontWeight:'bold'
color:'#000'
},
order_date: {
// fontSize:27,
// fontWeight:'bold',
fontSize:17,
// textAlign:'right'
},
product_name: {
fontSize:17,
fontWeight: 'bold',
// color:colors.darkgrey
},
product_price: {
// fontSize:17,
// fontWeight: 'bold'
textAlign:'right',
color:colors.darkgrey
},

add_address_btn_position: {
  position:'absolute',
 top:'1%',
  right:'5%',
  zIndex:10,
},
add_address_btn: {
//   backgroundColor: '#00afef',
  // alignItems:'center',
  borderRadius: 5,
    borderWidth:1,
  borderColor:colors.white,
  width: 30,
  height: 30,
  justifyContent:'center',
  
  
  
},
});
